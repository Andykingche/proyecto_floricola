package usuario.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ciudad.model.manager.ManagerCiudad;
import inventario.model.entities.Ciudad;
import inventario.model.entities.Usuario;
import mensaje.model.controller.JSFUtil;
import usuario.model.manager.ManagerUsuario;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped

public class BeanUsuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerUsuario managerUsuario;
	private ManagerCiudad managerCiudad;
	private List<Usuario> listaUsuarios;
	private Usuario usuario;
	private Ciudad ciudad;
	private boolean panelColapsado;
	private Usuario usuarioSeleccionado;

	@PostConstruct
	public void inicializar() {
		usuario = new Usuario();
		listaUsuarios = managerUsuario.findAllUsuarios();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarUsuario() {
		try {
			managerUsuario.insertarUsuario(usuario);
			listaUsuarios = managerUsuario.findAllUsuarios();
			usuario = new Usuario();
			JSFUtil.crearMensajeInfo("Usuario registrado.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarUsuario(int id_usuario) {
		managerUsuario.eliminarUsuario(id_usuario);
		listaUsuarios = managerUsuario.findAllUsuarios();
		JSFUtil.crearMensajeInfo("Usuario eliminado");
	}

	public void actionListenerSeleccionarUsuario(Usuario usuario) {
		usuarioSeleccionado = usuario;
	}

	public void actionListenerActualizarUsuario() {
		try {
			managerUsuario.actualizarUsuario(usuarioSeleccionado);
			listaUsuarios = managerUsuario.findAllUsuarios();
			JSFUtil.crearMensajeInfo("Datos actualizados");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
	

	
}
