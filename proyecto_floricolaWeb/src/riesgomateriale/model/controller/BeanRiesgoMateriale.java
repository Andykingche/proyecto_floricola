package riesgomateriale.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.RiesgoMateriale;
import riesgomateriale.model.manager.ManagerRiesgoMateriale;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanRiesgoMateriale implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerRiesgoMateriale managerriesgomateriale;
	
	private RiesgoMateriale riesgomateriale;
	private RiesgoMateriale riesgomaterialeSeleccionado;
	private List <RiesgoMateriale> listaRiesgoMateriale;
	private boolean panelColapsado;
	
	@PostConstruct
	public void inicializar () {
		riesgomateriale = new RiesgoMateriale();
		listaRiesgoMateriale = managerriesgomateriale.findAllRiegoMateriale();
		panelColapsado = true;
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}
	
	public void actionInsertarRiesgoMateriale () {
		try {
			managerriesgomateriale.insertRiesgoMateriale(riesgomateriale);
			listaRiesgoMateriale = managerriesgomateriale.findAllRiegoMateriale();
			riesgomateriale = new RiesgoMateriale();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionEliminarRiesgoMateriale (int id) {
		managerriesgomateriale.deleteRiesgoMateriale(id);
		listaRiesgoMateriale = managerriesgomateriale.findAllRiegoMateriale();
	}
	
	public void actionListenerSeleccionarRiesgoMateriale(RiesgoMateriale riesgomateriale) {
		riesgomaterialeSeleccionado = riesgomateriale;
	}
	
	public void actionListenerActualizarRiesgoMateriale () {
		try {
			managerriesgomateriale.updateRiesgoMateriale(riesgomaterialeSeleccionado);
			listaRiesgoMateriale = managerriesgomateriale.findAllRiegoMateriale();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public RiesgoMateriale getRiesgomateriale() {
		return riesgomateriale;
	}

	public void setRiesgomateriale(RiesgoMateriale riesgomateriale) {
		this.riesgomateriale = riesgomateriale;
	}

	public RiesgoMateriale getRiesgomaterialeSeleccionado() {
		return riesgomaterialeSeleccionado;
	}

	public void setRiesgomaterialeSeleccionado(RiesgoMateriale riesgomaterialeSeleccionado) {
		this.riesgomaterialeSeleccionado = riesgomaterialeSeleccionado;
	}

	public List<RiesgoMateriale> getListaRiesgoMateriale() {
		return listaRiesgoMateriale;
	}

	public void setListaRiesgoMateriale(List<RiesgoMateriale> listaRiesgoMateriale) {
		this.listaRiesgoMateriale = listaRiesgoMateriale;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

}
