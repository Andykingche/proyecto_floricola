package ciudad.model.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ciudad.model.manager.ManagerCiudad;
import inventario.model.entities.Ciudad;
import mensaje.model.controller.JSFUtil;


@Named
@SessionScoped
public class BeanCiudad implements Serializable{
	private static final long serialVersionUID=1L;
	
	@EJB
	private ManagerCiudad manageCiudad;
	private List<Ciudad> listaciudad;
	private Ciudad ciudad;
	private boolean panelColapsado;
	private Ciudad Ciudadseleccionada;
	
	@PostConstruct
	public void inicializar() 
	{
		ciudad = new Ciudad();
		listaciudad=manageCiudad.findAllCiudad();
		panelColapsado=true;
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarCiudad() {
		try {
			manageCiudad.insertarCiudad(ciudad);
			listaciudad = manageCiudad.findAllCiudad();
			ciudad = new Ciudad();
			JSFUtil.crearMensajeInfo("Ciudad registrada.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCiudad(int id_ciudad) {
		manageCiudad.eliminarCiudad(id_ciudad);
		listaciudad = manageCiudad.findAllCiudad();
		JSFUtil.crearMensajeInfo("Ciudad eliminada");
	}

	public void actionListenerSeleccionarUsuario(Ciudad ciudad) {
		Ciudadseleccionada = ciudad;
	}

	public void actionListenerActualizarCiudad() {
		try {
			manageCiudad.actualizarCiudad(Ciudadseleccionada);
			listaciudad = manageCiudad.findAllCiudad();
			JSFUtil.crearMensajeInfo("Datos actualizados");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Ciudad> getListaciudad() {
		return listaciudad;
	}

	public void setListaciudad(List<Ciudad> listaciudad) {
		this.listaciudad = listaciudad;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Ciudad getCiudadseleccionada() {
		return Ciudadseleccionada;
	}

	public void setCiudadseleccionada(Ciudad ciudadseleccionada) {
		Ciudadseleccionada = ciudadseleccionada;
	}	
	

}
