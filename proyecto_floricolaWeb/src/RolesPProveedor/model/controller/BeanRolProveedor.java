package RolesPProveedor.model.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.RolProveedor;
import mensaje.model.controller.JSFUtil;
import proveedor.model.manager.ManagerRolProveedor;

@Named
@SessionScoped
public class BeanRolProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerRolProveedor managerolproveedor;

	private RolProveedor rproveedor;
	private RolProveedor RolSeleccionado;
	private List<RolProveedor> listaProveedor;
	private boolean panelColapsado;

	@PostConstruct
	public void inicializar() {

		rproveedor = new RolProveedor();
		listaProveedor = managerolproveedor.findAllRolProveedor();
		panelColapsado = true;

	}
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionInsertarRolProveedor() {
		try {
			managerolproveedor.insertarRolProveedor(rproveedor);
			listaProveedor = managerolproveedor.findAllRolProveedor();
			rproveedor=new RolProveedor();
			JSFUtil.crearMensajeInfo("Rol de proveedor insertados.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());

			e.printStackTrace();
		}
	}

	public void actionListenerEliminarRolProveedor(int id) {
		managerolproveedor.eliminarRolProveedor(id);
		listaProveedor = managerolproveedor.findAllRolProveedor();
		JSFUtil.crearMensajeInfo("Rol de proveedor elminado");

	}

	public void actionListenerSeleccionarRolProveedor(RolProveedor rproveedor) {
		RolSeleccionado = rproveedor;
	}

	public void actionListenerActualizarRolProveedor() {
		try {
			managerolproveedor.actualizarRolProveedor(RolSeleccionado);
			listaProveedor = managerolproveedor.findAllRolProveedor();
			JSFUtil.crearMensajeInfo("Los datos han sido actualizados");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public RolProveedor getRproveedor() {
		return rproveedor;
	}

	public void setRproveedor(RolProveedor rproveedor) {
		this.rproveedor = rproveedor;
	}

	public RolProveedor getRolSeleccionado() {
		return RolSeleccionado;
	}

	public void setRolSeleccionado(RolProveedor rolSeleccionado) {
		RolSeleccionado = rolSeleccionado;
	}

	public List<RolProveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<RolProveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

}
