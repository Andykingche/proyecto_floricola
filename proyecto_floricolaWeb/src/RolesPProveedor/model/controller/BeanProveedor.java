package RolesPProveedor.model.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.Proveedor;
import inventario.model.entities.RolProveedor;
import mensaje.model.controller.JSFUtil;
import proveedor.model.manager.ManagerProveedor;

@Named
@SessionScoped
public class BeanProveedor implements Serializable{
	private static final long serialVersionUID=1L;
	@EJB
	private ManagerProveedor managerproveedor;
	
	private Proveedor proveedor;
	private Proveedor ProveedorSeleccionado;
	private RolProveedor rolproveedorSeleccionado;
	private List<Proveedor> listaProveedor;
	private List<RolProveedor> listaRolProveedor;
	private boolean panelColapsado;
	
	
	@PostConstruct
	public void inicializar() {

		proveedor = new Proveedor();
		listaProveedor = managerproveedor.findAllProveedor();
		listaRolProveedor=managerproveedor.findAllRolProveedor();
		panelColapsado = true;

	}
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionInsertarProveedor(RolProveedor rproveedor) {
		try {
			managerproveedor.insertarProveedor(proveedor);
			listaProveedor = managerproveedor.findAllProveedor();
			proveedor=new Proveedor();
			JSFUtil.crearMensajeInfo("Proveedor insertado correctamente.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());

			e.printStackTrace();
		}
	}

	public void actionListenerEliminarProveedor(int id) {
		managerproveedor.eliminarProveedor(id);
		listaProveedor = managerproveedor.findAllProveedor();
		JSFUtil.crearMensajeInfo("Proveedor elminado");

	}

	public void actionListenerSeleccionarProveedor(Proveedor proveedor) {
		ProveedorSeleccionado = proveedor;
	}

	public void actionListenerActualizarProveedor() {
		try {
			managerproveedor.actualizarProveedor(ProveedorSeleccionado);
			listaProveedor = managerproveedor.findAllProveedor();
			JSFUtil.crearMensajeInfo("Los datos han sido actualizados");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public Proveedor getProveedorSeleccionado() {
		return ProveedorSeleccionado;
	}
	public void setProveedorSeleccionado(Proveedor proveedorSeleccionado) {
		ProveedorSeleccionado = proveedorSeleccionado;
	}
	public List<Proveedor> getListaProveedor() {
		return listaProveedor;
	}
	public void setListaProveedor(List<Proveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}
	public boolean isPanelColapsado() {
		return panelColapsado;
	}
	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	public List<RolProveedor> getListaRolProveedor() {
		return listaRolProveedor;
	}
	public void setListaRolProveedor(List<RolProveedor> listaRolProveedor) {
		this.listaRolProveedor = listaRolProveedor;
	}
	
	
	
}
