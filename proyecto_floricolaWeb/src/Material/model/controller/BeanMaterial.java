package Material.model.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.Materiale;
import material.model.manager.ManagerMaterial;
import mensaje.model.controller.JSFUtil;

@Named
@SessionScoped
public class BeanMaterial implements Serializable {

	private static final long serialVersionUID=1L;
	
	@EJB
	private ManagerMaterial managermaterial;
	
	private Materiale materiales;
	private Materiale MaterialSeleccionado;
	private List<Materiale> listamateriales;
	private boolean panelColapsado;
	@PostConstruct
	public void inicializar() {
		materiales = new Materiale();
		listamateriales= managermaterial.findAllMateriales();
		panelColapsado=true;
		
	}
	
	
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionInsertarMaterial() {
		try {
			managermaterial.insertarMaterial(materiales);
			listamateriales = managermaterial.findAllMateriales();
			materiales=new Materiale();
			JSFUtil.crearMensajeInfo("Material insertado.");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());

			e.printStackTrace();
		}
	}

	public void actionListenerEliminarMaterial(int id) {
		managermaterial.eliminarMaterial(id);
		listamateriales= managermaterial.findAllMateriales();
		JSFUtil.crearMensajeInfo("Material elminado");

	}

	public void actionListenerSeleccionarMaterial(Materiale materiales) {
		MaterialSeleccionado = materiales;
	}

	public void actionListenerActualizarMaterial() {
		try {
			managermaterial.actualizarMaterial(MaterialSeleccionado);
			listamateriales = managermaterial.findAllMateriales();
			JSFUtil.crearMensajeInfo("Los datos han sido actualizados");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}


	public Materiale getMateriales() {
		return materiales;
	}


	public void setMateriales(Materiale materiales) {
		this.materiales = materiales;
	}


	public Materiale getMaterialSeleccionado() {
		return MaterialSeleccionado;
	}


	public void setMaterialSeleccionado(Materiale materialSeleccionado) {
		MaterialSeleccionado = materialSeleccionado;
	}


	public List<Materiale> getListamateriales() {
		return listamateriales;
	}


	public void setListamateriales(List<Materiale> listamateriales) {
		this.listamateriales = listamateriales;
	}


	public boolean isPanelColapsado() {
		return panelColapsado;
	}


	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	
}
