package bitacora.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import bitacora.model.manager.ManagerBitacora;
import inventario.model.entities.Bitacora;
import mensaje.model.controller.JSFUtil;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanBitacora implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerBitacora managerBitacora;
	private List<Bitacora> listaBitacora;
	private Bitacora bitacora;
	private boolean panelColapsado;
	private Bitacora bitacoraSeleccionada;

	@PostConstruct
	public void inicializar() {
		bitacora = new Bitacora();
		listaBitacora = managerBitacora.findAllBitacora();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarBitacora() {
		try {
			managerBitacora.insertarBitacora(bitacora);
			listaBitacora = managerBitacora.findAllBitacora();
			bitacora = new Bitacora();
			JSFUtil.crearMensajeInfo("Bitacora registrada.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarbitacora(int codigo_evento) {
		managerBitacora.eliminarBitacora(codigo_evento);
		listaBitacora = managerBitacora.findAllBitacora();
		JSFUtil.crearMensajeInfo("Rol eliminado");
	}

	public void actionListenerSeleccionarBitacora(Bitacora codigo_evento) {
		bitacoraSeleccionada = codigo_evento;
	}

	public void actionListenerActualizarBitacora() {
		try {
			managerBitacora.actualizarBitacora(bitacoraSeleccionada);
			listaBitacora = managerBitacora.findAllBitacora();
			JSFUtil.crearMensajeInfo("Datos actualizados");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Bitacora> getListaBitacora() {
		return listaBitacora;
	}

	public void setListaBitacora(List<Bitacora> listaBitacora) {
		this.listaBitacora = listaBitacora;
	}

	public Bitacora getBitacora() {
		return bitacora;
	}

	public void setBitacora(Bitacora bitacora) {
		this.bitacora = bitacora;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Bitacora getBitacoraSeleccionada() {
		return bitacoraSeleccionada;
	}

	public void setBitacoraSeleccionada(Bitacora bitacoraSeleccionada) {
		this.bitacoraSeleccionada = bitacoraSeleccionada;
	}

}
