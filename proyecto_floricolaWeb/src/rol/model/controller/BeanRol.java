package rol.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.Role;
import mensaje.model.controller.JSFUtil;
import roles.model.manager.ManagerRoles;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanRol implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerRoles managerRoles;
	private List<Role> listaRoles;
	private Role rol;
	private boolean panelColapsado;
	private Role rolSeleccionado;

	@PostConstruct
	public void inicializar() {
		rol = new Role();
		listaRoles = managerRoles.findAllRoles();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarRol() {
		try {
			managerRoles.insertarRol(rol);
			listaRoles = managerRoles.findAllRoles();
			rol = new Role();
			JSFUtil.crearMensajeInfo("Rol registrado.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarRol(int id_rol) {
		managerRoles.eliminarRol(id_rol);
		listaRoles = managerRoles.findAllRoles();
		JSFUtil.crearMensajeInfo("Rol eliminado");
	}

	public void actionListenerSeleccionarRol(Role rol) {
		rolSeleccionado = rol;
	}

	public void actionListenerActualizarRol() {
		try {
			managerRoles.actualizarRol(rolSeleccionado);
			listaRoles = managerRoles.findAllRoles();
			JSFUtil.crearMensajeInfo("Datos actualizados");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Role> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<Role> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public Role getRol() {
		return rol;
	}

	public void setRol(Role rol) {
		this.rol = rol;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Role getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Role rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}

}
