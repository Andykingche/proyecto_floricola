package tipomedida.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import inventario.model.entities.TipoMedida;
import tipomedida.model.manager.ManagerTipoMedida;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanTipoMedida implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerTipoMedida managertipomedida;
	
	private TipoMedida tipomedida;
	private TipoMedida tipomedidaSeleccionado;
	private List <TipoMedida> listaTipoMedida;
	private boolean panelColapsado;
	
	@PostConstruct
	public void inicializar () {
		tipomedida = new TipoMedida();
		listaTipoMedida = managertipomedida.findAllTipoMedida();
		panelColapsado = true;
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}
	
	public void actionInsertarTipoMedida () {
		try {
			managertipomedida.insertTipoMedida(tipomedida);
			listaTipoMedida = managertipomedida.findAllTipoMedida();
			tipomedida= new TipoMedida();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionEliminarTipoMedida (int id) {
		managertipomedida.deleteTipoMedida(id);
		listaTipoMedida = managertipomedida.findAllTipoMedida();
	}
	
	public void actionListenerSeleccionarTipoMedida(TipoMedida tipomedida) {
		tipomedidaSeleccionado = tipomedida;
	}
	
	public void actionListenerActualizarTipoMedida () {
		try {
			managertipomedida.updateTipoMedida(tipomedidaSeleccionado);
			listaTipoMedida = managertipomedida.findAllTipoMedida();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public TipoMedida getTipomedida() {
	
		if(tipomedida==null) {
			tipomedida=new TipoMedida();
			
		}
		return tipomedida;
	}

	public void setTipomedida(TipoMedida tipomedida) {
		this.tipomedida = tipomedida;
	}

	public TipoMedida getTipomedidaSeleccionado() {
		return tipomedidaSeleccionado;
	}

	public void setTipomedidaSeleccionado(TipoMedida tipomedidaSeleccionado) {
		this.tipomedidaSeleccionado = tipomedidaSeleccionado;
	}

	public List<TipoMedida> getListaTipoMedida() {
		return listaTipoMedida;
	}

	public void setListaTipoMedida(List<TipoMedida> listaTipoMedida) {
		this.listaTipoMedida = listaTipoMedida;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	
	
	
}
