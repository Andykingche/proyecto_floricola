package asignar.rol.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import asignar.rol.model.manager.ManagerAsigarRol;
import inventario.model.entities.AsignarRole;
import inventario.model.entities.Role;
import inventario.model.entities.Usuario;
import mensaje.model.controller.JSFUtil;
import roles.model.manager.ManagerRoles;
import usuario.model.manager.ManagerUsuario;

import java.io.Serializable;
import java.util.List;


@Named
@SessionScoped
public class BeanAsignarRol implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerAsigarRol managerAsignarRol;
	@EJB
	private ManagerUsuario managerUsuario;
	private List<AsignarRole> listaAsignarRoles;
	private List<Role> listaRoles;
	private List<Usuario> listaUsuario;
	private Usuario usuarioSeleccionado;
	private Role roleSeleccionado; 
	private AsignarRole asignarRol;
	private boolean panelColapsado;
	private AsignarRole rolSeleccionado;

	@PostConstruct
	public void inicializar() {
		asignarRol = new AsignarRole();
		listaAsignarRoles = managerAsignarRol.findAllAsignarRoles();
		panelColapsado = true;
	}
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}
	public void actionListenerInsertarAsignacionRol(int u, int r) {
		try {
			managerAsignarRol.insertarAsignarRol(u,r);
			listaAsignarRoles = managerAsignarRol.findAllAsignarRoles();
			asignarRol = new AsignarRole();
			JSFUtil.crearMensajeInfo("Asignacion registrada.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarRolAsignado(int id_asignar) {
		managerAsignarRol.eliminarAsignarRol(id_asignar);
		listaAsignarRoles = managerAsignarRol.findAllAsignarRoles();
		JSFUtil.crearMensajeInfo("Asignacion eliminada");
	}

	public void actionListenerSeleccionarRolAsignado(AsignarRole asignar) {
		rolSeleccionado = asignar;
	}
	public void actionListenerSeleccionarUsuario(Usuario usuario) {
		usuarioSeleccionado = usuario;
	}

	public void actionListenerActualizarRolAsignado() {
		try {
			managerAsignarRol.actualizarAsignarRol(rolSeleccionado);
			listaAsignarRoles = managerAsignarRol.findAllAsignarRoles();
			JSFUtil.crearMensajeInfo("Datos actualizados");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	public List<AsignarRole> getListaAsignarRoles() {
		return listaAsignarRoles;
	}
	public void setListaAsignarRoles(List<AsignarRole> listaAsignarRoles) {
		this.listaAsignarRoles = listaAsignarRoles;
	}
	public List<Role> getListaRoles() {
		return listaRoles;
	}
	public void setListaRoles(List<Role> listaRoles) {
		this.listaRoles = listaRoles;
	}
	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}
	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	public AsignarRole getAsignarRol() {
		return asignarRol;
	}
	public void setAsignarRol(AsignarRole asignarRol) {
		this.asignarRol = asignarRol;
	}
	public boolean isPanelColapsado() {
		return panelColapsado;
	}
	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	public AsignarRole getRolSeleccionado() {
		return rolSeleccionado;
	}
	public void setRolSeleccionado(AsignarRole rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}
	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}
	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}
	public Role getRoleSeleccionado() {
		return roleSeleccionado;
	}
	public void setRoleSeleccionado(Role roleSeleccionado) {
		this.roleSeleccionado = roleSeleccionado;
	}
	
	
}
