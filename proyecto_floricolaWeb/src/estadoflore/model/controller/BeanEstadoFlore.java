package estadoflore.model.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import estadoflore.model.manager.ManagerEstadoFlore;
import inventario.model.entities.EstadoFlore;
import mensaje.model.controller.JSFUtil;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanEstadoFlore implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerEstadoFlore managerestadoflore;

	private EstadoFlore estadoflore;
	private EstadoFlore estadofloreSeleccionado;
	private List<EstadoFlore> listaEstadoFlore;
	private boolean panelColapsado;

	@PostConstruct
	public void inicializar() {
		estadoflore = new EstadoFlore();
		listaEstadoFlore = managerestadoflore.findAllEstadoFlore();
		panelColapsado = true;
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionInsertarEstadoFlore() {
		try {
			managerestadoflore.insertEstadoFlore(estadoflore);
			listaEstadoFlore = managerestadoflore.findAllEstadoFlore();
			estadoflore=new EstadoFlore();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actionEliminarEstadoFlore(int id) {
		managerestadoflore.deleteEstadoFlore(id);
		listaEstadoFlore = managerestadoflore.findAllEstadoFlore();
	}

	public void actionListenerSeleccionarEstadoFlore(EstadoFlore estadoflore) {
		estadofloreSeleccionado = estadoflore;
	}

	public void actionListenerActualizarEstadoFlore() {
		try {
			managerestadoflore.updateEstadoFlore(estadofloreSeleccionado);
			listaEstadoFlore = managerestadoflore.findAllEstadoFlore();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EstadoFlore getEstadoflore() {
		return estadoflore;
	}

	public void setEstadoflore(EstadoFlore estadoflore) {
		this.estadoflore = estadoflore;
	}

	public EstadoFlore getEstadofloreSeleccionado() {
		return estadofloreSeleccionado;
	}

	public void setEstadofloreSeleccionado(EstadoFlore estadofloreSeleccionado) {
		this.estadofloreSeleccionado = estadofloreSeleccionado;
	}

	public List<EstadoFlore> getListaEstadoFlore() {
		return listaEstadoFlore;
	}

	public void setListaEstadoFlore(List<EstadoFlore> listaEstadoFlore) {
		this.listaEstadoFlore = listaEstadoFlore;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

}
