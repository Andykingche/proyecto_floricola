package estadoflore.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.EstadoFlore;

/**
 * Session Bean implementation class ManagerEstadoFlore
 */
@Stateless
@LocalBean
public class ManagerEstadoFlore {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerEstadoFlore() {
        // TODO Auto-generated constructor stub
    }
    
    public List <EstadoFlore> findAllEstadoFlore(){
    	return em.createNamedQuery("EstadoFlore.findAll").getResultList();
    }
    
    public EstadoFlore findEstadoFloresById (int id) {
    	return em.find(EstadoFlore.class, id);
    }
    
    public void insertEstadoFlore (EstadoFlore estadoflore) throws Exception {
    	em.persist(estadoflore);
    }
    
    public void deleteEstadoFlore (int id) {
		EstadoFlore estadoflore = findEstadoFloresById(id);
		if (estadoflore != null)
			em.remove(estadoflore);
	}
    
    public void updateEstadoFlore (EstadoFlore estadoflore) throws Exception {
		EstadoFlore e = findEstadoFloresById(estadoflore.getIdEstadoFlores());
		if (e == null)
			throw new Exception("No existe ningun estado flor con el id especificado");
		e.setNombreEstadoFlores(estadoflore.getNombreEstadoFlores());
		em.merge(e);
	}
}
