package asignar.rol.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.AsignarRole;
import inventario.model.entities.Role;
import inventario.model.entities.Usuario;

/**
 * Session Bean implementation class ManagerAsigarRol
 */
@Stateless
@LocalBean
public class ManagerAsigarRol {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerAsigarRol() {
        // TODO Auto-generated constructor stub
    }
    
    public List<AsignarRole> findAllAsignarRoles() {

		return em.createNamedQuery("AsignarRole.findAll").getResultList();

	}

	public AsignarRole findAsignarRolesById(int id_asignar) {
		return em.find(AsignarRole.class, id_asignar);
	}

	public void insertarAsignarRol(int id_usuario,int id_rol) throws Exception {

		AsignarRole ar = new AsignarRole();
		Usuario  us = em.find(Usuario.class,id_usuario); 
		
		ar.setUsuario(us);
		Role rol = em.find(Role.class, id_rol);
		ar.setRole(rol);
		em.persist(ar);
		
	}

	public void eliminarAsignarRol(int id_asignar) {
		AsignarRole asignarRol = findAsignarRolesById(id_asignar);
		if (asignarRol != null)
			em.remove(asignarRol);
	}

	public void actualizarAsignarRol(AsignarRole asignar) throws Exception {
		AsignarRole a = findAsignarRolesById(asignar.getIdAsignar());
		if (a == null)
			throw new Exception("No existe la asignacion con el codigo especificado");
		a.setUsuario(asignar.getUsuario());
		a.setRole(asignar.getRole());
		em.merge(a);
	}


}
