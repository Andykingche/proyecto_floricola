package riesgomateriale.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.RiesgoMateriale;


/**
 * Session Bean implementation class ManagerRiesgoMateriale
 */
@Stateless
@LocalBean
public class ManagerRiesgoMateriale {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerRiesgoMateriale() {
        // TODO Auto-generated constructor stub
    }
    
    public List <RiesgoMateriale> findAllRiegoMateriale(){
    	return em.createNamedQuery("RiesgoMateriale.findAll").getResultList();
    }
    
    public RiesgoMateriale findRiesgoMaterialeById (int id) {
    	return em.find(RiesgoMateriale.class, id);
    }
    
    public void insertRiesgoMateriale (RiesgoMateriale riesgomateriale) throws Exception {
    	em.persist(riesgomateriale);
    }
    
    public void deleteRiesgoMateriale (int id) {
		RiesgoMateriale riesgomateriale = findRiesgoMaterialeById(id);
		if (riesgomateriale != null)
			em.remove(riesgomateriale);
	}
    
    public void updateRiesgoMateriale (RiesgoMateriale riesgomateriale) throws Exception {
		RiesgoMateriale r = findRiesgoMaterialeById(riesgomateriale.getIdRiesgomateriales());
		if (r == null)
			throw new Exception("No existe ningun riesgo material con el id especificado");
		r.setNombre(riesgomateriale.getNombre());
		r.setDescripcion(riesgomateriale.getDescripcion());
		em.merge(r);
	}
}
