package usuario.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import inventario.model.entities.Usuario;

/**
 * Session Bean implementation class ManagerUsuario
 */
@Stateless
@LocalBean
public class ManagerUsuario {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerUsuario() {
        // TODO Auto-generated constructor stub
    }
    public List<Usuario> findAllUsuarios() {

  		
  		return em.createNamedQuery("Usuario.findAll").getResultList();
  		
  	}
 
  	public Usuario findUsuarioById(int id_usuario) {
  		return em.find(Usuario.class, id_usuario);
  	}

  	public void insertarUsuario(Usuario usuario) throws Exception {
  	
  		em.persist(usuario);
  	}

  	public void eliminarUsuario(int id_usuario) {
  		Usuario usuario= findUsuarioById(id_usuario);
  		if (usuario != null)
  			em.remove(usuario);
  	}

  	public void actualizarUsuario(Usuario usuario) throws Exception {
  		Usuario u = findUsuarioById(usuario.getIdUsuario());
  		if (u == null)
  			throw new Exception("No existe el Usuario con el codigo especificado");
  		u.setNombreUsuario(usuario.getNombreUsuario());
  		u.setPasswordUsuario(usuario.getPasswordUsuario());
  		u.setTelefonoUsuario(usuario.getTelefonoUsuario());
  		u.setDireccionUsuario(usuario.getDireccionUsuario());
  		em.merge(u);
  	}

}
