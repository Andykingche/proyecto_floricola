package ciudad.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import inventario.model.entities.Ciudad;

/**
 * Session Bean implementation class ManagerCiudad
 */
@Stateless
@LocalBean
public class ManagerCiudad {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	public ManagerCiudad() {
		// TODO Auto-generated constructor stub
	}

	public List<Ciudad>findAllCiudad(){
	
	return em.createNamedQuery("Ciudad.findAll").getResultList();
	}
	
	public Ciudad findCiudadById(int id_ciudad) {
		return em.find(Ciudad.class, id_ciudad);
	}

	public void insertarCiudad(Ciudad ciudad) throws Exception {

		em.persist(ciudad);
	}

	public void eliminarCiudad(int id_ciudad) {
		Ciudad ciudad = findCiudadById(id_ciudad);
		if (ciudad != null)
			em.remove(ciudad);
	}

	public void actualizarCiudad(Ciudad ciudad) throws Exception {
		Ciudad c = findCiudadById(ciudad.getIdCiudad());
		if (c == null)
			throw new Exception("No existe la asignacion con el codigo especificado");
		c.setNombreCiudad(ciudad.getNombreCiudad());
		em.merge(c);
	}
}
