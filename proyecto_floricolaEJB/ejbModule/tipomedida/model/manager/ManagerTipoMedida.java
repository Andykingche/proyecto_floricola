package tipomedida.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.TipoMedida;

/**
 * Session Bean implementation class ManagerTipoMedida
 */
@Stateless
@LocalBean
public class ManagerTipoMedida {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerTipoMedida() {
        // TODO Auto-generated constructor stub
    }
    
    public List <TipoMedida> findAllTipoMedida(){
    	return em.createNamedQuery("TipoMedida.findAll").getResultList();
    }
    
    public TipoMedida findTipoMedidaById (int id) {
    	return em.find(TipoMedida.class, id);
    }
    
    public void insertTipoMedida (TipoMedida tipomedida) throws Exception {
    	em.persist(tipomedida);
    }
    
    public void deleteTipoMedida (int id) {
		TipoMedida tipomedida = findTipoMedidaById(id);
		if (tipomedida != null)
			em.remove(tipomedida);
	}
    
    public void updateTipoMedida (TipoMedida tipomedida) throws Exception {
		TipoMedida t = findTipoMedidaById(tipomedida.getIdMedida());
		if (t == null)
			throw new Exception("No existe ningun tipo medida con el id especificado");
		t.setNombreMedida(tipomedida.getNombreMedida());
		t.setSiglaMedida(tipomedida.getSiglaMedida());
		em.merge(t);
	}

}
