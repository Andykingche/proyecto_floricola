package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name="roles")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rol", unique=true, nullable=false)
	private Integer idRol;

	@Column(name="nombre_rol", nullable=false, length=50)
	private String nombreRol;

	//bi-directional many-to-one association to AsignarRole
	@OneToMany(mappedBy="role")
	private List<AsignarRole> asignarRoles;

	public Role() {
	}

	public Integer getIdRol() {
		return this.idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public String getNombreRol() {
		return this.nombreRol;
	}

	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

	public List<AsignarRole> getAsignarRoles() {
		return this.asignarRoles;
	}

	public void setAsignarRoles(List<AsignarRole> asignarRoles) {
		this.asignarRoles = asignarRoles;
	}

	public AsignarRole addAsignarRole(AsignarRole asignarRole) {
		getAsignarRoles().add(asignarRole);
		asignarRole.setRole(this);

		return asignarRole;
	}

	public AsignarRole removeAsignarRole(AsignarRole asignarRole) {
		getAsignarRoles().remove(asignarRole);
		asignarRole.setRole(null);

		return asignarRole;
	}

}