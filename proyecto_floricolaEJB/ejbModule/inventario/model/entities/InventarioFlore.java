package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the inventario_flores database table.
 * 
 */
@Entity
@Table(name="inventario_flores")
@NamedQuery(name="InventarioFlore.findAll", query="SELECT i FROM InventarioFlore i")
public class InventarioFlore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_flores", unique=true, nullable=false)
	private Integer idInvFlores;

	@Column(name="cantidad_actual", nullable=false, precision=131089)
	private BigDecimal cantidadActual;

	@Column(name="fecha_inv_flores", nullable=false)
	private Timestamp fechaInvFlores;

	@Column(name="stock_max", nullable=false, precision=131089)
	private BigDecimal stockMax;

	@Column(name="stock_min", nullable=false, precision=131089)
	private BigDecimal stockMin;

	//bi-directional many-to-one association to Clasificacion
	@ManyToOne
	@JoinColumn(name="id_clasificacion_clasificacion")
	private Clasificacion clasificacion;

	//bi-directional many-to-one association to Longitude
	@ManyToOne
	@JoinColumn(name="id_longitud_longitudes")
	private Longitude longitude;

	//bi-directional many-to-one association to TipoVenta
	@ManyToOne
	@JoinColumn(name="id_tipo_venta_tipo_venta")
	private TipoVenta tipoVenta;

	public InventarioFlore() {
	}

	public Integer getIdInvFlores() {
		return this.idInvFlores;
	}

	public void setIdInvFlores(Integer idInvFlores) {
		this.idInvFlores = idInvFlores;
	}

	public BigDecimal getCantidadActual() {
		return this.cantidadActual;
	}

	public void setCantidadActual(BigDecimal cantidadActual) {
		this.cantidadActual = cantidadActual;
	}

	public Timestamp getFechaInvFlores() {
		return this.fechaInvFlores;
	}

	public void setFechaInvFlores(Timestamp fechaInvFlores) {
		this.fechaInvFlores = fechaInvFlores;
	}

	public BigDecimal getStockMax() {
		return this.stockMax;
	}

	public void setStockMax(BigDecimal stockMax) {
		this.stockMax = stockMax;
	}

	public BigDecimal getStockMin() {
		return this.stockMin;
	}

	public void setStockMin(BigDecimal stockMin) {
		this.stockMin = stockMin;
	}

	public Clasificacion getClasificacion() {
		return this.clasificacion;
	}

	public void setClasificacion(Clasificacion clasificacion) {
		this.clasificacion = clasificacion;
	}

	public Longitude getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Longitude longitude) {
		this.longitude = longitude;
	}

	public TipoVenta getTipoVenta() {
		return this.tipoVenta;
	}

	public void setTipoVenta(TipoVenta tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

}