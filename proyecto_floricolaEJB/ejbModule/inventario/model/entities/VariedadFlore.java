package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the variedad_flores database table.
 * 
 */
@Entity
@Table(name="variedad_flores")
@NamedQuery(name="VariedadFlore.findAll", query="SELECT v FROM VariedadFlore v")
public class VariedadFlore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_variedad_flores", unique=true, nullable=false)
	private Integer idVariedadFlores;

	@Column(name="nombre_variedad_flores", nullable=false, length=200)
	private String nombreVariedadFlores;

	//bi-directional many-to-one association to PedidoFlore
	@OneToMany(mappedBy="variedadFlore")
	private List<PedidoFlore> pedidoFlores;

	public VariedadFlore() {
	}

	public Integer getIdVariedadFlores() {
		return this.idVariedadFlores;
	}

	public void setIdVariedadFlores(Integer idVariedadFlores) {
		this.idVariedadFlores = idVariedadFlores;
	}

	public String getNombreVariedadFlores() {
		return this.nombreVariedadFlores;
	}

	public void setNombreVariedadFlores(String nombreVariedadFlores) {
		this.nombreVariedadFlores = nombreVariedadFlores;
	}

	public List<PedidoFlore> getPedidoFlores() {
		return this.pedidoFlores;
	}

	public void setPedidoFlores(List<PedidoFlore> pedidoFlores) {
		this.pedidoFlores = pedidoFlores;
	}

	public PedidoFlore addPedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().add(pedidoFlore);
		pedidoFlore.setVariedadFlore(this);

		return pedidoFlore;
	}

	public PedidoFlore removePedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().remove(pedidoFlore);
		pedidoFlore.setVariedadFlore(null);

		return pedidoFlore;
	}

}