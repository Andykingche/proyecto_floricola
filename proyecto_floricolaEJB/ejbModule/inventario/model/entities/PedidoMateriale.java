package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pedido_materiales database table.
 * 
 */
@Entity
@Table(name="pedido_materiales")
@NamedQuery(name="PedidoMateriale.findAll", query="SELECT p FROM PedidoMateriale p")
public class PedidoMateriale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pedido_materiales", unique=true, nullable=false)
	private Integer idPedidoMateriales;

	@Column(name="cantidad_pedido_materiales", nullable=false)
	private Integer cantidadPedidoMateriales;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_entrega_materiales", nullable=false)
	private Date fechaEntregaMateriales;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_pedido_materiales", nullable=false)
	private Date fechaPedidoMateriales;

	@Column(name="valor_pedido_materiales")
	private double valorPedidoMateriales;

	//bi-directional many-to-one association to Materiale
	@ManyToOne
	@JoinColumn(name="id_materiales_materiales")
	private Materiale materiale;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name="id_proveedor_proveedor")
	private Proveedor proveedor;

	//bi-directional many-to-one association to TipoMedida
	@ManyToOne
	@JoinColumn(name="id_medida_tipo_medida")
	private TipoMedida tipoMedida;

	public PedidoMateriale() {
	}

	public Integer getIdPedidoMateriales() {
		return this.idPedidoMateriales;
	}

	public void setIdPedidoMateriales(Integer idPedidoMateriales) {
		this.idPedidoMateriales = idPedidoMateriales;
	}

	public Integer getCantidadPedidoMateriales() {
		return this.cantidadPedidoMateriales;
	}

	public void setCantidadPedidoMateriales(Integer cantidadPedidoMateriales) {
		this.cantidadPedidoMateriales = cantidadPedidoMateriales;
	}

	public Date getFechaEntregaMateriales() {
		return this.fechaEntregaMateriales;
	}

	public void setFechaEntregaMateriales(Date fechaEntregaMateriales) {
		this.fechaEntregaMateriales = fechaEntregaMateriales;
	}

	public Date getFechaPedidoMateriales() {
		return this.fechaPedidoMateriales;
	}

	public void setFechaPedidoMateriales(Date fechaPedidoMateriales) {
		this.fechaPedidoMateriales = fechaPedidoMateriales;
	}

	public double getValorPedidoMateriales() {
		return this.valorPedidoMateriales;
	}

	public void setValorPedidoMateriales(double valorPedidoMateriales) {
		this.valorPedidoMateriales = valorPedidoMateriales;
	}

	public Materiale getMateriale() {
		return this.materiale;
	}

	public void setMateriale(Materiale materiale) {
		this.materiale = materiale;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public TipoMedida getTipoMedida() {
		return this.tipoMedida;
	}

	public void setTipoMedida(TipoMedida tipoMedida) {
		this.tipoMedida = tipoMedida;
	}

}