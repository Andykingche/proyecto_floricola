package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_venta database table.
 * 
 */
@Entity
@Table(name="tipo_venta")
@NamedQuery(name="TipoVenta.findAll", query="SELECT t FROM TipoVenta t")
public class TipoVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_venta", unique=true, nullable=false)
	private Integer idTipoVenta;

	@Column(name="nombre_tipo_venta", nullable=false, length=50)
	private String nombreTipoVenta;

	//bi-directional many-to-one association to InventarioFlore
	@OneToMany(mappedBy="tipoVenta")
	private List<InventarioFlore> inventarioFlores;

	public TipoVenta() {
	}

	public Integer getIdTipoVenta() {
		return this.idTipoVenta;
	}

	public void setIdTipoVenta(Integer idTipoVenta) {
		this.idTipoVenta = idTipoVenta;
	}

	public String getNombreTipoVenta() {
		return this.nombreTipoVenta;
	}

	public void setNombreTipoVenta(String nombreTipoVenta) {
		this.nombreTipoVenta = nombreTipoVenta;
	}

	public List<InventarioFlore> getInventarioFlores() {
		return this.inventarioFlores;
	}

	public void setInventarioFlores(List<InventarioFlore> inventarioFlores) {
		this.inventarioFlores = inventarioFlores;
	}

	public InventarioFlore addInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().add(inventarioFlore);
		inventarioFlore.setTipoVenta(this);

		return inventarioFlore;
	}

	public InventarioFlore removeInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().remove(inventarioFlore);
		inventarioFlore.setTipoVenta(null);

		return inventarioFlore;
	}

}