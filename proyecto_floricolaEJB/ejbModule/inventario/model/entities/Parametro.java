package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the parametros database table.
 * 
 */
@Entity
@Table(name="parametros")
@NamedQuery(name="Parametro.findAll", query="SELECT p FROM Parametro p")
public class Parametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_parametro", unique=true, nullable=false)
	private Integer idParametro;

	@Column(name="nombre_parametro", nullable=false, length=100)
	private String nombreParametro;

	@Column(name="valor_parametro", nullable=false, precision=131089)
	private BigDecimal valorParametro;

	public Parametro() {
	}

	public Integer getIdParametro() {
		return this.idParametro;
	}

	public void setIdParametro(Integer idParametro) {
		this.idParametro = idParametro;
	}

	public String getNombreParametro() {
		return this.nombreParametro;
	}

	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}

	public BigDecimal getValorParametro() {
		return this.valorParametro;
	}

	public void setValorParametro(BigDecimal valorParametro) {
		this.valorParametro = valorParametro;
	}

}