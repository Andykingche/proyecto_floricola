package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the bitacora database table.
 * 
 */
@Entity
@Table(name="bitacora")
@NamedQuery(name="Bitacora.findAll", query="SELECT b FROM Bitacora b")
public class Bitacora implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codigo_evento", unique=true, nullable=false)
	private Integer codigoEvento;

	@Column(name="descripcion_bitacora", nullable=false, length=200)
	private String descripcionBitacora;

	@Column(name="\"direccionIP_bitacora\"", nullable=false, length=20)
	private String direccionIP_bitacora;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_evento", nullable=false)
	private Date fechaEvento;

	@Column(name="metodo_bitacora", nullable=false, length=100)
	private String metodoBitacora;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario_usuarios")
	private Usuario usuario;

	public Bitacora() {
	}

	public Integer getCodigoEvento() {
		return this.codigoEvento;
	}

	public void setCodigoEvento(Integer codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public String getDescripcionBitacora() {
		return this.descripcionBitacora;
	}

	public void setDescripcionBitacora(String descripcionBitacora) {
		this.descripcionBitacora = descripcionBitacora;
	}

	public String getDireccionIP_bitacora() {
		return this.direccionIP_bitacora;
	}

	public void setDireccionIP_bitacora(String direccionIP_bitacora) {
		this.direccionIP_bitacora = direccionIP_bitacora;
	}

	public Date getFechaEvento() {
		return this.fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getMetodoBitacora() {
		return this.metodoBitacora;
	}

	public void setMetodoBitacora(String metodoBitacora) {
		this.metodoBitacora = metodoBitacora;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}