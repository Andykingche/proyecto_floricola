package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the materiales database table.
 * 
 */
@Entity
@Table(name="materiales")
@NamedQuery(name="Materiale.findAll", query="SELECT m FROM Materiale m")
public class Materiale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_materiales", unique=true, nullable=false)
	private Integer idMateriales;

	@Column(length=200)
	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_caducidad", nullable=false)
	private Date fechaCaducidad;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_elaboracion", nullable=false)
	private Date fechaElaboracion;

	@Column(nullable=false, length=100)
	private String nombre;

	//bi-directional many-to-one association to InventarioMateriale
	@OneToMany(mappedBy="materiale")
	private List<InventarioMateriale> inventarioMateriales;

	//bi-directional many-to-one association to RiesgoMateriale
	@ManyToOne
	@JoinColumn(name="id_riesgomateriales_riesgo_materiales")
	private RiesgoMateriale riesgoMateriale;

	//bi-directional many-to-one association to PedidoMateriale
	@OneToMany(mappedBy="materiale")
	private List<PedidoMateriale> pedidoMateriales;

	public Materiale() {
	}

	public Integer getIdMateriales() {
		return this.idMateriales;
	}

	public void setIdMateriales(Integer idMateriales) {
		this.idMateriales = idMateriales;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCaducidad() {
		return this.fechaCaducidad;
	}

	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public Date getFechaElaboracion() {
		return this.fechaElaboracion;
	}

	public void setFechaElaboracion(Date fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<InventarioMateriale> getInventarioMateriales() {
		return this.inventarioMateriales;
	}

	public void setInventarioMateriales(List<InventarioMateriale> inventarioMateriales) {
		this.inventarioMateriales = inventarioMateriales;
	}

	public InventarioMateriale addInventarioMateriale(InventarioMateriale inventarioMateriale) {
		getInventarioMateriales().add(inventarioMateriale);
		inventarioMateriale.setMateriale(this);

		return inventarioMateriale;
	}

	public InventarioMateriale removeInventarioMateriale(InventarioMateriale inventarioMateriale) {
		getInventarioMateriales().remove(inventarioMateriale);
		inventarioMateriale.setMateriale(null);

		return inventarioMateriale;
	}

	public RiesgoMateriale getRiesgoMateriale() {
		return this.riesgoMateriale;
	}

	public void setRiesgoMateriale(RiesgoMateriale riesgoMateriale) {
		this.riesgoMateriale = riesgoMateriale;
	}

	public List<PedidoMateriale> getPedidoMateriales() {
		return this.pedidoMateriales;
	}

	public void setPedidoMateriales(List<PedidoMateriale> pedidoMateriales) {
		this.pedidoMateriales = pedidoMateriales;
	}

	public PedidoMateriale addPedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().add(pedidoMateriale);
		pedidoMateriale.setMateriale(this);

		return pedidoMateriale;
	}

	public PedidoMateriale removePedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().remove(pedidoMateriale);
		pedidoMateriale.setMateriale(null);

		return pedidoMateriale;
	}

}