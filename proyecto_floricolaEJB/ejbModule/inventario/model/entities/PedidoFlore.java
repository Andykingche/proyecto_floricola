package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pedido_flores database table.
 * 
 */
@Entity
@Table(name="pedido_flores")
@NamedQuery(name="PedidoFlore.findAll", query="SELECT p FROM PedidoFlore p")
public class PedidoFlore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pedido_flores", unique=true, nullable=false)
	private Integer idPedidoFlores;

	@Column(name="cantidad_tallos_pedido", nullable=false, precision=50)
	private BigDecimal cantidadTallosPedido;

	@Column(name="fecha_entrega_flores", nullable=false)
	private Timestamp fechaEntregaFlores;

	@Column(name="fecha_pedido_flores", nullable=false)
	private Timestamp fechaPedidoFlores;

	@Column(name="valor_pedido_flores")
	private double valorPedidoFlores;

	//bi-directional many-to-one association to Clasificacion
	@OneToMany(mappedBy="pedidoFlore")
	private List<Clasificacion> clasificacions;

	//bi-directional many-to-one association to Longitude
	@ManyToOne
	@JoinColumn(name="id_longitud_longitudes")
	private Longitude longitude;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name="id_proveedor_proveedor")
	private Proveedor proveedor;

	//bi-directional many-to-one association to VariedadFlore
	@ManyToOne
	@JoinColumn(name="id_variedad_flores_variedad_flores")
	private VariedadFlore variedadFlore;

	public PedidoFlore() {
	}

	public Integer getIdPedidoFlores() {
		return this.idPedidoFlores;
	}

	public void setIdPedidoFlores(Integer idPedidoFlores) {
		this.idPedidoFlores = idPedidoFlores;
	}

	public BigDecimal getCantidadTallosPedido() {
		return this.cantidadTallosPedido;
	}

	public void setCantidadTallosPedido(BigDecimal cantidadTallosPedido) {
		this.cantidadTallosPedido = cantidadTallosPedido;
	}

	public Timestamp getFechaEntregaFlores() {
		return this.fechaEntregaFlores;
	}

	public void setFechaEntregaFlores(Timestamp fechaEntregaFlores) {
		this.fechaEntregaFlores = fechaEntregaFlores;
	}

	public Timestamp getFechaPedidoFlores() {
		return this.fechaPedidoFlores;
	}

	public void setFechaPedidoFlores(Timestamp fechaPedidoFlores) {
		this.fechaPedidoFlores = fechaPedidoFlores;
	}

	public double getValorPedidoFlores() {
		return this.valorPedidoFlores;
	}

	public void setValorPedidoFlores(double valorPedidoFlores) {
		this.valorPedidoFlores = valorPedidoFlores;
	}

	public List<Clasificacion> getClasificacions() {
		return this.clasificacions;
	}

	public void setClasificacions(List<Clasificacion> clasificacions) {
		this.clasificacions = clasificacions;
	}

	public Clasificacion addClasificacion(Clasificacion clasificacion) {
		getClasificacions().add(clasificacion);
		clasificacion.setPedidoFlore(this);

		return clasificacion;
	}

	public Clasificacion removeClasificacion(Clasificacion clasificacion) {
		getClasificacions().remove(clasificacion);
		clasificacion.setPedidoFlore(null);

		return clasificacion;
	}

	public Longitude getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Longitude longitude) {
		this.longitude = longitude;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public VariedadFlore getVariedadFlore() {
		return this.variedadFlore;
	}

	public void setVariedadFlore(VariedadFlore variedadFlore) {
		this.variedadFlore = variedadFlore;
	}

}