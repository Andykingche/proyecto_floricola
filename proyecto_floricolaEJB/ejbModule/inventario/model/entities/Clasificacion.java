package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the clasificacion database table.
 * 
 */
@Entity
@Table(name="clasificacion")
@NamedQuery(name="Clasificacion.findAll", query="SELECT c FROM Clasificacion c")
public class Clasificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_clasificacion", unique=true, nullable=false)
	private Integer idClasificacion;

	@Column(name="cantidad_clasificacion", nullable=false, precision=10)
	private BigDecimal cantidadClasificacion;

	//bi-directional many-to-one association to EstadoFlore
	@ManyToOne
	@JoinColumn(name="id_estado_flores_estado_flores")
	private EstadoFlore estadoFlore;

	//bi-directional many-to-one association to PedidoFlore
	@ManyToOne
	@JoinColumn(name="id_pedido_flores_pedido_flores")
	private PedidoFlore pedidoFlore;

	//bi-directional many-to-one association to InventarioFlore
	@OneToMany(mappedBy="clasificacion")
	private List<InventarioFlore> inventarioFlores;

	public Clasificacion() {
	}

	public Integer getIdClasificacion() {
		return this.idClasificacion;
	}

	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}

	public BigDecimal getCantidadClasificacion() {
		return this.cantidadClasificacion;
	}

	public void setCantidadClasificacion(BigDecimal cantidadClasificacion) {
		this.cantidadClasificacion = cantidadClasificacion;
	}

	public EstadoFlore getEstadoFlore() {
		return this.estadoFlore;
	}

	public void setEstadoFlore(EstadoFlore estadoFlore) {
		this.estadoFlore = estadoFlore;
	}

	public PedidoFlore getPedidoFlore() {
		return this.pedidoFlore;
	}

	public void setPedidoFlore(PedidoFlore pedidoFlore) {
		this.pedidoFlore = pedidoFlore;
	}

	public List<InventarioFlore> getInventarioFlores() {
		return this.inventarioFlores;
	}

	public void setInventarioFlores(List<InventarioFlore> inventarioFlores) {
		this.inventarioFlores = inventarioFlores;
	}

	public InventarioFlore addInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().add(inventarioFlore);
		inventarioFlore.setClasificacion(this);

		return inventarioFlore;
	}

	public InventarioFlore removeInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().remove(inventarioFlore);
		inventarioFlore.setClasificacion(null);

		return inventarioFlore;
	}

}