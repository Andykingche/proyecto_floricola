package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the usuarios database table.
 * 
 */
@Entity
@Table(name="usuarios")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario", unique=true, nullable=false)
	private Integer idUsuario;

	@Column(name="apellido_usuario", nullable=false, length=100)
	private String apellidoUsuario;

	@Column(name="cedula_usuario", nullable=false, length=11)
	private String cedulaUsuario;

	@Column(name="correo_usuario", length=200)
	private String correoUsuario;

	@Column(name="direccion_usuario", length=250)
	private String direccionUsuario;

	@Column(name="estado_civil_usuario", length=50)
	private String estadoCivilUsuario;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_nacimiento_usuario", nullable=false)
	private Date fechaNacimientoUsuario;

	@Column(name="genero_usuario", nullable=false, length=20)
	private String generoUsuario;

	@Column(name="nombre_usuario", nullable=false, length=100)
	private String nombreUsuario;

	@Column(name="password_usuario", nullable=false, length=100)
	private String passwordUsuario;

	@Column(name="telefono_usuario", length=10)
	private String telefonoUsuario;

	//bi-directional many-to-one association to AsignarRole
	@OneToMany(mappedBy="usuario")
	private List<AsignarRole> asignarRoles;

	//bi-directional many-to-one association to Bitacora
	@OneToMany(mappedBy="usuario")
	private List<Bitacora> bitacoras;

	//bi-directional many-to-one association to Ciudad
	@ManyToOne
	@JoinColumn(name="id_ciudad_ciudad")
	private Ciudad ciudad;

	public Usuario() {
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getApellidoUsuario() {
		return this.apellidoUsuario;
	}

	public void setApellidoUsuario(String apellidoUsuario) {
		this.apellidoUsuario = apellidoUsuario;
	}

	public String getCedulaUsuario() {
		return this.cedulaUsuario;
	}

	public void setCedulaUsuario(String cedulaUsuario) {
		this.cedulaUsuario = cedulaUsuario;
	}

	public String getCorreoUsuario() {
		return this.correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getDireccionUsuario() {
		return this.direccionUsuario;
	}

	public void setDireccionUsuario(String direccionUsuario) {
		this.direccionUsuario = direccionUsuario;
	}

	public String getEstadoCivilUsuario() {
		return this.estadoCivilUsuario;
	}

	public void setEstadoCivilUsuario(String estadoCivilUsuario) {
		this.estadoCivilUsuario = estadoCivilUsuario;
	}

	public Date getFechaNacimientoUsuario() {
		return this.fechaNacimientoUsuario;
	}

	public void setFechaNacimientoUsuario(Date fechaNacimientoUsuario) {
		this.fechaNacimientoUsuario = fechaNacimientoUsuario;
	}

	public String getGeneroUsuario() {
		return this.generoUsuario;
	}

	public void setGeneroUsuario(String generoUsuario) {
		this.generoUsuario = generoUsuario;
	}

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPasswordUsuario() {
		return this.passwordUsuario;
	}

	public void setPasswordUsuario(String passwordUsuario) {
		this.passwordUsuario = passwordUsuario;
	}

	public String getTelefonoUsuario() {
		return this.telefonoUsuario;
	}

	public void setTelefonoUsuario(String telefonoUsuario) {
		this.telefonoUsuario = telefonoUsuario;
	}

	public List<AsignarRole> getAsignarRoles() {
		return this.asignarRoles;
	}

	public void setAsignarRoles(List<AsignarRole> asignarRoles) {
		this.asignarRoles = asignarRoles;
	}

	public AsignarRole addAsignarRole(AsignarRole asignarRole) {
		getAsignarRoles().add(asignarRole);
		asignarRole.setUsuario(this);

		return asignarRole;
	}

	public AsignarRole removeAsignarRole(AsignarRole asignarRole) {
		getAsignarRoles().remove(asignarRole);
		asignarRole.setUsuario(null);

		return asignarRole;
	}

	public List<Bitacora> getBitacoras() {
		return this.bitacoras;
	}

	public void setBitacoras(List<Bitacora> bitacoras) {
		this.bitacoras = bitacoras;
	}

	public Bitacora addBitacora(Bitacora bitacora) {
		getBitacoras().add(bitacora);
		bitacora.setUsuario(this);

		return bitacora;
	}

	public Bitacora removeBitacora(Bitacora bitacora) {
		getBitacoras().remove(bitacora);
		bitacora.setUsuario(null);

		return bitacora;
	}

	public Ciudad getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

}