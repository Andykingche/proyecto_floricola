package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the estado_flores database table.
 * 
 */
@Entity
@Table(name="estado_flores")
@NamedQuery(name="EstadoFlore.findAll", query="SELECT e FROM EstadoFlore e")
public class EstadoFlore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estado_flores", unique=true, nullable=false)
	private Integer idEstadoFlores;

	@Column(name="nombre_estado_flores", nullable=false, length=50)
	private String nombreEstadoFlores;

	//bi-directional many-to-one association to Clasificacion
	@OneToMany(mappedBy="estadoFlore")
	private List<Clasificacion> clasificacions;

	public EstadoFlore() {
	}

	public Integer getIdEstadoFlores() {
		return this.idEstadoFlores;
	}

	public void setIdEstadoFlores(Integer idEstadoFlores) {
		this.idEstadoFlores = idEstadoFlores;
	}

	public String getNombreEstadoFlores() {
		return this.nombreEstadoFlores;
	}

	public void setNombreEstadoFlores(String nombreEstadoFlores) {
		this.nombreEstadoFlores = nombreEstadoFlores;
	}

	public List<Clasificacion> getClasificacions() {
		return this.clasificacions;
	}

	public void setClasificacions(List<Clasificacion> clasificacions) {
		this.clasificacions = clasificacions;
	}

	public Clasificacion addClasificacion(Clasificacion clasificacion) {
		getClasificacions().add(clasificacion);
		clasificacion.setEstadoFlore(this);

		return clasificacion;
	}

	public Clasificacion removeClasificacion(Clasificacion clasificacion) {
		getClasificacions().remove(clasificacion);
		clasificacion.setEstadoFlore(null);

		return clasificacion;
	}

}