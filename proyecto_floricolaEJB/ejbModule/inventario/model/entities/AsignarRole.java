package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the asignar_roles database table.
 * 
 */
@Entity
@Table(name="asignar_roles")
@NamedQuery(name="AsignarRole.findAll", query="SELECT a FROM AsignarRole a")
public class AsignarRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignar", unique=true, nullable=false)
	private Integer idAsignar;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="id_rol_roles")
	private Role role;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario_usuarios")
	private Usuario usuario;

	public AsignarRole() {
	}

	public Integer getIdAsignar() {
		return this.idAsignar;
	}

	public void setIdAsignar(Integer idAsignar) {
		this.idAsignar = idAsignar;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}