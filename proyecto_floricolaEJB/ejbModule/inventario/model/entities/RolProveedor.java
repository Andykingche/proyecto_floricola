package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the rol_proveedor database table.
 * 
 */
@Entity
@Table(name="rol_proveedor")
@NamedQuery(name="RolProveedor.findAll", query="SELECT r FROM RolProveedor r")
public class RolProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rol_proveedor", unique=true, nullable=false)
	private Integer idRolProveedor;

	@Column(name="nombre_rol_proveedor", nullable=false, length=50)
	private String nombreRolProveedor;

	//bi-directional many-to-one association to Proveedor
	@OneToMany(mappedBy="rolProveedor")
	private List<Proveedor> proveedors;

	public RolProveedor() {
	}

	public Integer getIdRolProveedor() {
		return this.idRolProveedor;
	}

	public void setIdRolProveedor(Integer idRolProveedor) {
		this.idRolProveedor = idRolProveedor;
	}

	public String getNombreRolProveedor() {
		return this.nombreRolProveedor;
	}

	public void setNombreRolProveedor(String nombreRolProveedor) {
		this.nombreRolProveedor = nombreRolProveedor;
	}

	public List<Proveedor> getProveedors() {
		return this.proveedors;
	}

	public void setProveedors(List<Proveedor> proveedors) {
		this.proveedors = proveedors;
	}

	public Proveedor addProveedor(Proveedor proveedor) {
		getProveedors().add(proveedor);
		proveedor.setRolProveedor(this);

		return proveedor;
	}

	public Proveedor removeProveedor(Proveedor proveedor) {
		getProveedors().remove(proveedor);
		proveedor.setRolProveedor(null);

		return proveedor;
	}

}