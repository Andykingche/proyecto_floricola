package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_medida database table.
 * 
 */
@Entity
@Table(name="tipo_medida")
@NamedQuery(name="TipoMedida.findAll", query="SELECT t FROM TipoMedida t")
public class TipoMedida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_medida", unique=true, nullable=false)
	private Integer idMedida;

	@Column(name="nombre_medida", nullable=false, length=50)
	private String nombreMedida;

	@Column(name="sigla_medida", nullable=false, length=10)
	private String siglaMedida;

	//bi-directional many-to-one association to PedidoMateriale
	@OneToMany(mappedBy="tipoMedida")
	private List<PedidoMateriale> pedidoMateriales;

	public TipoMedida() {
	}

	public Integer getIdMedida() {
		return this.idMedida;
	}

	public void setIdMedida(Integer idMedida) {
		this.idMedida = idMedida;
	}

	public String getNombreMedida() {
		return this.nombreMedida;
	}

	public void setNombreMedida(String nombreMedida) {
		this.nombreMedida = nombreMedida;
	}

	public String getSiglaMedida() {
		return this.siglaMedida;
	}

	public void setSiglaMedida(String siglaMedida) {
		this.siglaMedida = siglaMedida;
	}

	public List<PedidoMateriale> getPedidoMateriales() {
		return this.pedidoMateriales;
	}

	public void setPedidoMateriales(List<PedidoMateriale> pedidoMateriales) {
		this.pedidoMateriales = pedidoMateriales;
	}

	public PedidoMateriale addPedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().add(pedidoMateriale);
		pedidoMateriale.setTipoMedida(this);

		return pedidoMateriale;
	}

	public PedidoMateriale removePedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().remove(pedidoMateriale);
		pedidoMateriale.setTipoMedida(null);

		return pedidoMateriale;
	}

}