package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the proveedor database table.
 * 
 */
@Entity
@Table(name="proveedor")
@NamedQuery(name="Proveedor.findAll", query="SELECT p FROM Proveedor p")
public class Proveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_proveedor", unique=true, nullable=false)
	private Integer idProveedor;

	@Column(name="apellido_proveedor", nullable=false, length=100)
	private String apellidoProveedor;

	@Column(name="correo_proveedor", nullable=false, length=100)
	private String correoProveedor;

	@Column(name="direccion_proveedor", nullable=false, length=150)
	private String direccionProveedor;

	@Column(name="nombre_proveedor", nullable=false, length=100)
	private String nombreProveedor;

	@Column(name="ruc_proveedor", nullable=false, length=15)
	private String rucProveedor;

	@Column(name="telefono_proveedor", nullable=false, length=10)
	private String telefonoProveedor;

	//bi-directional many-to-one association to PedidoFlore
	@OneToMany(mappedBy="proveedor")
	private List<PedidoFlore> pedidoFlores;

	//bi-directional many-to-one association to PedidoMateriale
	@OneToMany(mappedBy="proveedor")
	private List<PedidoMateriale> pedidoMateriales;

	//bi-directional one-to-one association to Ciudad
	@OneToOne
	@JoinColumn(name="id_proveedor", nullable=false, insertable=false, updatable=false)
	private Ciudad ciudad1;

	//bi-directional many-to-one association to Ciudad
	@ManyToOne
	@JoinColumn(name="id_ciudad_ciudad")
	private Ciudad ciudad2;

	//bi-directional many-to-one association to RolProveedor
	@ManyToOne
	@JoinColumn(name="id_rol_proveedor_rol_proveedor")
	private RolProveedor rolProveedor;

	public Proveedor() {
	}

	public Integer getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getApellidoProveedor() {
		return this.apellidoProveedor;
	}

	public void setApellidoProveedor(String apellidoProveedor) {
		this.apellidoProveedor = apellidoProveedor;
	}

	public String getCorreoProveedor() {
		return this.correoProveedor;
	}

	public void setCorreoProveedor(String correoProveedor) {
		this.correoProveedor = correoProveedor;
	}

	public String getDireccionProveedor() {
		return this.direccionProveedor;
	}

	public void setDireccionProveedor(String direccionProveedor) {
		this.direccionProveedor = direccionProveedor;
	}

	public String getNombreProveedor() {
		return this.nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getRucProveedor() {
		return this.rucProveedor;
	}

	public void setRucProveedor(String rucProveedor) {
		this.rucProveedor = rucProveedor;
	}

	public String getTelefonoProveedor() {
		return this.telefonoProveedor;
	}

	public void setTelefonoProveedor(String telefonoProveedor) {
		this.telefonoProveedor = telefonoProveedor;
	}

	public List<PedidoFlore> getPedidoFlores() {
		return this.pedidoFlores;
	}

	public void setPedidoFlores(List<PedidoFlore> pedidoFlores) {
		this.pedidoFlores = pedidoFlores;
	}

	public PedidoFlore addPedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().add(pedidoFlore);
		pedidoFlore.setProveedor(this);

		return pedidoFlore;
	}

	public PedidoFlore removePedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().remove(pedidoFlore);
		pedidoFlore.setProveedor(null);

		return pedidoFlore;
	}

	public List<PedidoMateriale> getPedidoMateriales() {
		return this.pedidoMateriales;
	}

	public void setPedidoMateriales(List<PedidoMateriale> pedidoMateriales) {
		this.pedidoMateriales = pedidoMateriales;
	}

	public PedidoMateriale addPedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().add(pedidoMateriale);
		pedidoMateriale.setProveedor(this);

		return pedidoMateriale;
	}

	public PedidoMateriale removePedidoMateriale(PedidoMateriale pedidoMateriale) {
		getPedidoMateriales().remove(pedidoMateriale);
		pedidoMateriale.setProveedor(null);

		return pedidoMateriale;
	}

	public Ciudad getCiudad1() {
		return this.ciudad1;
	}

	public void setCiudad1(Ciudad ciudad1) {
		this.ciudad1 = ciudad1;
	}

	public Ciudad getCiudad2() {
		return this.ciudad2;
	}

	public void setCiudad2(Ciudad ciudad2) {
		this.ciudad2 = ciudad2;
	}

	public RolProveedor getRolProveedor() {
		return this.rolProveedor;
	}

	public void setRolProveedor(RolProveedor rolProveedor) {
		this.rolProveedor = rolProveedor;
	}

}