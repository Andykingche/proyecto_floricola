package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the longitudes database table.
 * 
 */
@Entity
@Table(name="longitudes")
@NamedQuery(name="Longitude.findAll", query="SELECT l FROM Longitude l")
public class Longitude implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_longitud", unique=true, nullable=false)
	private Integer idLongitud;

	@Column(name="cantidad_longitud", nullable=false, precision=20)
	private BigDecimal cantidadLongitud;

	@Column(name="sigla_longitud", nullable=false, length=5)
	private String siglaLongitud;

	//bi-directional many-to-one association to InventarioFlore
	@OneToMany(mappedBy="longitude")
	private List<InventarioFlore> inventarioFlores;

	//bi-directional many-to-one association to InventarioMateriale
	@OneToMany(mappedBy="longitude")
	private List<InventarioMateriale> inventarioMateriales;

	//bi-directional many-to-one association to PedidoFlore
	@OneToMany(mappedBy="longitude")
	private List<PedidoFlore> pedidoFlores;

	public Longitude() {
	}

	public Integer getIdLongitud() {
		return this.idLongitud;
	}

	public void setIdLongitud(Integer idLongitud) {
		this.idLongitud = idLongitud;
	}

	public BigDecimal getCantidadLongitud() {
		return this.cantidadLongitud;
	}

	public void setCantidadLongitud(BigDecimal cantidadLongitud) {
		this.cantidadLongitud = cantidadLongitud;
	}

	public String getSiglaLongitud() {
		return this.siglaLongitud;
	}

	public void setSiglaLongitud(String siglaLongitud) {
		this.siglaLongitud = siglaLongitud;
	}

	public List<InventarioFlore> getInventarioFlores() {
		return this.inventarioFlores;
	}

	public void setInventarioFlores(List<InventarioFlore> inventarioFlores) {
		this.inventarioFlores = inventarioFlores;
	}

	public InventarioFlore addInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().add(inventarioFlore);
		inventarioFlore.setLongitude(this);

		return inventarioFlore;
	}

	public InventarioFlore removeInventarioFlore(InventarioFlore inventarioFlore) {
		getInventarioFlores().remove(inventarioFlore);
		inventarioFlore.setLongitude(null);

		return inventarioFlore;
	}

	public List<InventarioMateriale> getInventarioMateriales() {
		return this.inventarioMateriales;
	}

	public void setInventarioMateriales(List<InventarioMateriale> inventarioMateriales) {
		this.inventarioMateriales = inventarioMateriales;
	}

	public InventarioMateriale addInventarioMateriale(InventarioMateriale inventarioMateriale) {
		getInventarioMateriales().add(inventarioMateriale);
		inventarioMateriale.setLongitude(this);

		return inventarioMateriale;
	}

	public InventarioMateriale removeInventarioMateriale(InventarioMateriale inventarioMateriale) {
		getInventarioMateriales().remove(inventarioMateriale);
		inventarioMateriale.setLongitude(null);

		return inventarioMateriale;
	}

	public List<PedidoFlore> getPedidoFlores() {
		return this.pedidoFlores;
	}

	public void setPedidoFlores(List<PedidoFlore> pedidoFlores) {
		this.pedidoFlores = pedidoFlores;
	}

	public PedidoFlore addPedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().add(pedidoFlore);
		pedidoFlore.setLongitude(this);

		return pedidoFlore;
	}

	public PedidoFlore removePedidoFlore(PedidoFlore pedidoFlore) {
		getPedidoFlores().remove(pedidoFlore);
		pedidoFlore.setLongitude(null);

		return pedidoFlore;
	}

}