package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the inventario_materiales database table.
 * 
 */
@Entity
@Table(name="inventario_materiales")
@NamedQuery(name="InventarioMateriale.findAll", query="SELECT i FROM InventarioMateriale i")
public class InventarioMateriale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_materiales", unique=true, nullable=false)
	private Integer idInvMateriales;

	@Column(name="cantidad_actual", nullable=false, precision=131089)
	private BigDecimal cantidadActual;

	@Column(name="fecha_inv_materiales", nullable=false)
	private Timestamp fechaInvMateriales;

	@Column(name="stock_max", nullable=false, precision=131089)
	private BigDecimal stockMax;

	@Column(name="stock_min", nullable=false, precision=131089)
	private BigDecimal stockMin;

	//bi-directional many-to-one association to Longitude
	@ManyToOne
	@JoinColumn(name="id_longitud_longitudes")
	private Longitude longitude;

	//bi-directional many-to-one association to Materiale
	@ManyToOne
	@JoinColumn(name="id_materiales_materiales")
	private Materiale materiale;

	public InventarioMateriale() {
	}

	public Integer getIdInvMateriales() {
		return this.idInvMateriales;
	}

	public void setIdInvMateriales(Integer idInvMateriales) {
		this.idInvMateriales = idInvMateriales;
	}

	public BigDecimal getCantidadActual() {
		return this.cantidadActual;
	}

	public void setCantidadActual(BigDecimal cantidadActual) {
		this.cantidadActual = cantidadActual;
	}

	public Timestamp getFechaInvMateriales() {
		return this.fechaInvMateriales;
	}

	public void setFechaInvMateriales(Timestamp fechaInvMateriales) {
		this.fechaInvMateriales = fechaInvMateriales;
	}

	public BigDecimal getStockMax() {
		return this.stockMax;
	}

	public void setStockMax(BigDecimal stockMax) {
		this.stockMax = stockMax;
	}

	public BigDecimal getStockMin() {
		return this.stockMin;
	}

	public void setStockMin(BigDecimal stockMin) {
		this.stockMin = stockMin;
	}

	public Longitude getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Longitude longitude) {
		this.longitude = longitude;
	}

	public Materiale getMateriale() {
		return this.materiale;
	}

	public void setMateriale(Materiale materiale) {
		this.materiale = materiale;
	}

}