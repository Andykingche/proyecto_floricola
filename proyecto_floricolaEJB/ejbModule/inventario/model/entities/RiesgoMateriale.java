package inventario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the riesgo_materiales database table.
 * 
 */
@Entity
@Table(name="riesgo_materiales")
@NamedQuery(name="RiesgoMateriale.findAll", query="SELECT r FROM RiesgoMateriale r")
public class RiesgoMateriale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_riesgomateriales", unique=true, nullable=false)
	private Integer idRiesgomateriales;

	@Column(length=200)
	private String descripcion;

	@Column(nullable=false, length=100)
	private String nombre;

	//bi-directional many-to-one association to Materiale
	@OneToMany(mappedBy="riesgoMateriale")
	private List<Materiale> materiales;

	public RiesgoMateriale() {
	}

	public Integer getIdRiesgomateriales() {
		return this.idRiesgomateriales;
	}

	public void setIdRiesgomateriales(Integer idRiesgomateriales) {
		this.idRiesgomateriales = idRiesgomateriales;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Materiale> getMateriales() {
		return this.materiales;
	}

	public void setMateriales(List<Materiale> materiales) {
		this.materiales = materiales;
	}

	public Materiale addMateriale(Materiale materiale) {
		getMateriales().add(materiale);
		materiale.setRiesgoMateriale(this);

		return materiale;
	}

	public Materiale removeMateriale(Materiale materiale) {
		getMateriales().remove(materiale);
		materiale.setRiesgoMateriale(null);

		return materiale;
	}

}