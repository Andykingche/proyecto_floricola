package bitacora.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.Bitacora;

/**
 * Session Bean implementation class ManagerBitacora
 */
@Stateless
@LocalBean
public class ManagerBitacora {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerBitacora() {
        // TODO Auto-generated constructor stub
    }
    public List<Bitacora> findAllBitacora() {

		return em.createNamedQuery("Bitacora.findAll").getResultList();

	}

	public Bitacora findBitacoraById(int codigo_evento) {
		return em.find(Bitacora.class, codigo_evento);
	}

	public void insertarBitacora(Bitacora bitacora) throws Exception {

		em.persist(bitacora);
	}

	public void eliminarBitacora(int codigo_evento) {
		Bitacora bitacora = findBitacoraById(codigo_evento);
		if (bitacora != null)
			em.remove(bitacora);
	}

	public void actualizarBitacora(Bitacora bitacora) throws Exception {
		Bitacora b = findBitacoraById(bitacora.getCodigoEvento());
		if (b == null)
			throw new Exception("No existe bitacora con el codigo especificado");
		b.setFechaEvento(bitacora.getFechaEvento());
		b.setMetodoBitacora(bitacora.getMetodoBitacora());
		b.setDescripcionBitacora(bitacora.getDescripcionBitacora());
		b.setDireccionIP_bitacora(bitacora.getDireccionIP_bitacora());
		em.merge(b);
	}


}
