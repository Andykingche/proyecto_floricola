package proveedor.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.RolProveedor;

/**
 * Session Bean implementation class ManagerRolProveedor
 */
@Stateless
@LocalBean
public class ManagerRolProveedor {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerRolProveedor() {
        // TODO Auto-generated constructor stub
    }
    public List<RolProveedor> findAllRolProveedor(){
    	
    	return em.createNamedQuery("RolProveedor.findAll").getResultList();   	
    }
    
    public RolProveedor findProveedorById(int id) {
		return em.find(RolProveedor.class, id);
	}
    
	public void insertarRolProveedor(RolProveedor rproveedor) throws Exception {
		
		em.persist(rproveedor);
	}
	
	public void eliminarRolProveedor(int id) {
		RolProveedor rproveedor = findProveedorById(id);
		if (rproveedor!= null)
			em.remove(rproveedor);
	}

	public void actualizarRolProveedor(RolProveedor rproveedor) throws Exception {
		RolProveedor r = findProveedorById(rproveedor.getIdRolProveedor());
		if (r== null)
			throw new Exception("No existe el rol Especificado");
		r.setNombreRolProveedor(rproveedor.getNombreRolProveedor());
		
		em.merge(r);
	}

}
