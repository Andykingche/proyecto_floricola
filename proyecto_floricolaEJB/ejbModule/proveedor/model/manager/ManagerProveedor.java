package proveedor.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.Proveedor;
import inventario.model.entities.RolProveedor;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerProveedor() {
        // TODO Auto-generated constructor stub
    }
    
public List<Proveedor> findAllProveedor(){
    	
    	return em.createNamedQuery("Proveedor.findAll").getResultList();   	
    }
public List<RolProveedor> findAllRolProveedor(){
	
	return em.createNamedQuery("RolProveedor.findAll").getResultList();   	
}
    public Proveedor findProveedorById(int id) {
		return em.find(Proveedor.class, id);
	}
    
	public void insertarProveedor(Proveedor proveedor) throws Exception {
		Proveedor p = new Proveedor();
		
		
		em.persist(proveedor);
	}
	
	public void eliminarProveedor(int id) {
		Proveedor proveedor = findProveedorById(id);
		if (proveedor!= null)
			em.remove(proveedor);
	}

	public void actualizarProveedor(Proveedor rproveedor) throws Exception {
		Proveedor r = findProveedorById(rproveedor.getIdProveedor());
		if (r== null)
			throw new Exception("No existe el rol Especificado");
		r.setNombreProveedor(rproveedor.getNombreProveedor());
		
		em.merge(r);
	}

}
