package roles.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.Role;
import sun.util.logging.resources.logging;

/**
 * Session Bean implementation class ManagerRoles
 */
@Stateless
@LocalBean
public class ManagerRoles {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerRoles() {
		// TODO Auto-generated constructor stub
	}

	public List<Role> findAllRoles() {

		return em.createNamedQuery("Role.findAll").getResultList();

	}

	public Role findRolesById(int id_rol) {
		return em.find(Role.class, id_rol);
	}

	public void insertarRol(Role rol) throws Exception {
		if (rol == null) {
			rol = new Role();
			em.persist(rol);
		}
		em.persist(rol);

	}

	public void eliminarRol(int id_rol) {
		Role rol = findRolesById(id_rol);
		if (rol != null)
			em.remove(rol);
	}

	public void actualizarRol(Role rol) throws Exception {
		Role r = findRolesById(rol.getIdRol());
		if (r == null)
			throw new Exception("No existe el Rol con el codigo especificado");
		r.setNombreRol(rol.getNombreRol());
		em.merge(r);
	}

}
