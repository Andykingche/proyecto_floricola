package login.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import asignar.rol.model.manager.ManagerAsigarRol;
import inventario.model.entities.AsignarRole;
import inventario.model.entities.Role;
import inventario.model.entities.Usuario;
import login.model.dto.LoginDTO;
import usuario.model.manager.ManagerUsuario;

/**
 * Session Bean implementation class ManagerSeguridad
 */
@Stateless
@LocalBean
public class ManagerSeguridad {
	@EJB
	private ManagerDAO managerDAO;
	@EJB
	private ManagerAuditoria managerAuditoria;
	@EJB
	private ManagerUsuario managerUsuario;
	@EJB
	private ManagerAsigarRol managerAsigarRol;
	
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerSeguridad() {
        // TODO Auto-generated constructor stub
    }
    
    
    /**
	 * Metodo que le permite a un usuario acceder al sistema.
	 * @param codigoUsuario Identificador del usuario.
	 * @param clave Clave de acceso.
	 * @return Objeto LoginDTO con informacion del usuario para la sesion.
	 * @throws Exception Cuando no coincide la clave proporcionada o si ocurrio
	 * un error con la consulta a la base de datos.
	 */
    public String devolver_rolusuario(int idrol){
    	Role rol = new Role();
    	
    	String nombrerol=" ";
    	String consulta=" ";
    	try {
    		
    		consulta = "FROM Role u WHERE u.idRol = ?1";
    		Query query = em.createQuery(consulta);
    		query.setParameter(1, idrol);
    		 List<Role>list = query.getResultList();
    		 if(!list.isEmpty()) {
    			 rol= list.get(0);
    			  nombrerol=rol.getNombreRol();
    			  
    			 
    		 }
    			 
    			 
    	}catch(Exception e) {
    		
    		
    	}
    	return nombrerol;
    	
    }
    
    public int devloveridrol(int iduser) {
    	int rol=0;
    	int rol2=0;
    	
    	AsignarRole usr = new AsignarRole();
    	Role r = new Role();
    	Usuario u= new Usuario();
    	String consulta=" ";
    	 try {
    		 
    		 consulta="SELECT u FROM AsignarRole u";
    		 Query query = em.createQuery(consulta);
    		 List<AsignarRole> list=query.getResultList();
    		 if(!list.isEmpty()) {
    			 
    			 usr=list.get(0);
    			 for(AsignarRole ar: list) {
    				 
    				 if(iduser==ar.getUsuario().getIdUsuario()) {
    					 rol2=ar.getRole().getIdRol();
    					 return rol2;
    					
    				 }
    			 }
    		 }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return rol2;
    }
    
	public LoginDTO accederSistema(String codigoUsuario,String clave) {
		Usuario usuario= new Usuario();
		String consulta;
		String nombreRol;
		int id_rol=0;
		LoginDTO loginDTO = new LoginDTO();
	
			consulta = "FROM Usuario u WHERE u.cedulaUsuario = ?1 and passwordUsuario = ?2";
			
			Query query = em.createQuery(consulta);
			query.setParameter(1, codigoUsuario);
			query.setParameter(2, clave);
			List<Usuario> listaUsuario = query.getResultList();
			
			if(!listaUsuario.isEmpty()) {
				usuario = listaUsuario.get(0);
				id_rol = devloveridrol(usuario.getIdUsuario());
				nombreRol = devolver_rolusuario(id_rol);
				loginDTO.setUsuario(usuario.getNombreUsuario());
				loginDTO.setCodigoUsuario(usuario.getCedulaUsuario());
				loginDTO.setPassword(usuario.getPasswordUsuario());
				loginDTO.setTipoUsuario(nombreRol);
				
				if(nombreRol.equals("Administrador")) {
					loginDTO.setRutaAcceso("/administrador/index.html");}
				if(nombreRol.equals("Supervisor")) {
					loginDTO.setRutaAcceso("/supervisor/index.html");}
				if(nombreRol.equals("Operador")) {
					loginDTO.setRutaAcceso("/operador/index.html");}
			//managerAuditoria.crearEvento(usuario.getCedulaUsuario(), ManagerSeguridad.class, "accederSistema", "Acceso al sistema para usuarios.");
		return loginDTO;
			}
		return loginDTO;
	}

	public Usuario findUsuarioById(String codigoUsuario) throws Exception {
		Usuario usuario=(Usuario)managerDAO.findById(Usuario.class, codigoUsuario);
		return usuario;
	}


}
