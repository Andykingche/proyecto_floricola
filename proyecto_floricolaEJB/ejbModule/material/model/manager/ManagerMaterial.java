package material.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import inventario.model.entities.Materiale;


/**
 * Session Bean implementation class Material
 */
@Stateless
@LocalBean
public class ManagerMaterial {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagerMaterial() {
        // TODO Auto-generated constructor stub
    }

    
 public List<Materiale> findAllMateriales(){
    	
    	return em.createNamedQuery("Materiale.findAll").getResultList();   	
    }
    
    public Materiale findMaterialesById(int id) {
		return em.find(Materiale.class, id);
	}
    
	public void insertarMaterial(Materiale material) throws Exception {
		
		em.persist(material);
	}
	
	public void eliminarMaterial(int id) {
		Materiale material = findMaterialesById(id);
		if (material!= null)
			em.remove(material);
	}

	public void actualizarMaterial(Materiale material) throws Exception {
		Materiale m = findMaterialesById(material.getIdMateriales());
		if (m== null)
			throw new Exception("No existe el rol Especificado");
		m.setNombre(material.getNombre());
		
		em.merge(m);
	}

}
