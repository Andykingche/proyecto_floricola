-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.1
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: gestion | type: DATABASE --
-- -- DROP DATABASE IF EXISTS gestion;
-- CREATE DATABASE gestion;
-- -- ddl-end --
-- 

-- object: public.usuarios | type: TABLE --
-- DROP TABLE IF EXISTS public.usuarios CASCADE;
CREATE TABLE public.usuarios(
	id_usuario serial NOT NULL,
	nombre_usuario varchar(100) NOT NULL,
	password_usuario varchar(100) NOT NULL,
	telefono_usuario varchar(10),
	direccion_usuario varchar(250),
	apellido_usuario varchar(100) NOT NULL,
	cedula_usuario varchar(11) NOT NULL,
	correo_usuario varchar(200),
	id_ciudad_ciudad integer,
	CONSTRAINT "USUARIOS_pk" PRIMARY KEY (id_usuario)

);
-- ddl-end --
COMMENT ON TABLE public.usuarios IS 'Tabla para registrar usuarios del sistema';
-- ddl-end --
COMMENT ON COLUMN public.usuarios.id_usuario IS 'clave pimaria de la tabla USUARIOS';
-- ddl-end --
COMMENT ON COLUMN public.usuarios.nombre_usuario IS 'almacena el nombre del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usuarios.password_usuario IS 'almacena la contraseña del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usuarios.telefono_usuario IS 'alamcena el numero telefónico del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usuarios.direccion_usuario IS 'alamacena la dirección del usuario';
-- ddl-end --
ALTER TABLE public.usuarios OWNER TO postgres;
-- ddl-end --

-- object: public.roles | type: TABLE --
-- DROP TABLE IF EXISTS public.roles CASCADE;
CREATE TABLE public.roles(
	id_rol serial NOT NULL,
	nombre_rol varchar(50) NOT NULL,
	CONSTRAINT "ROLES_pk" PRIMARY KEY (id_rol)

);
-- ddl-end --
COMMENT ON COLUMN public.roles.id_rol IS 'clave primaria de la tabla ROLES';
-- ddl-end --
COMMENT ON COLUMN public.roles.nombre_rol IS 'alamcena el nombre del rol';
-- ddl-end --
ALTER TABLE public.roles OWNER TO postgres;
-- ddl-end --

-- object: public.tipo_medida | type: TABLE --
-- DROP TABLE IF EXISTS public.tipo_medida CASCADE;
CREATE TABLE public.tipo_medida(
	id_medida serial NOT NULL,
	nombre_medida varchar(50) NOT NULL,
	sigla_medida varchar(10) NOT NULL,
	CONSTRAINT "TIPO_MEDIDA_pk" PRIMARY KEY (id_medida)

);
-- ddl-end --
COMMENT ON TABLE public.tipo_medida IS 'Alamcena la medida de los productos';
-- ddl-end --
ALTER TABLE public.tipo_medida OWNER TO postgres;
-- ddl-end --

-- object: public.pedido_materiales | type: TABLE --
-- DROP TABLE IF EXISTS public.pedido_materiales CASCADE;
CREATE TABLE public.pedido_materiales(
	id_pedido_materiales serial NOT NULL,
	fecha_pedido_materiales date NOT NULL,
	fecha_entrega_materiales date NOT NULL,
	cantidad_pedido_materiales integer NOT NULL,
	id_medida_tipo_medida integer,
	id_proveedor_proveedor integer,
	id_materiales_materiales integer,
	valor_pedido_materiales money,
	CONSTRAINT "PEDIDOS_pk" PRIMARY KEY (id_pedido_materiales)

);
-- ddl-end --
COMMENT ON TABLE public.pedido_materiales IS 'tabla que almacena todos los pedidos realizados por la empresa';
-- ddl-end --
COMMENT ON COLUMN public.pedido_materiales.id_pedido_materiales IS 'clave primaria de la tabla pedido';
-- ddl-end --
ALTER TABLE public.pedido_materiales OWNER TO postgres;
-- ddl-end --

-- object: tipo_medida_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_materiales DROP CONSTRAINT IF EXISTS tipo_medida_fk CASCADE;
ALTER TABLE public.pedido_materiales ADD CONSTRAINT tipo_medida_fk FOREIGN KEY (id_medida_tipo_medida)
REFERENCES public.tipo_medida (id_medida) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.proveedor | type: TABLE --
-- DROP TABLE IF EXISTS public.proveedor CASCADE;
CREATE TABLE public.proveedor(
	id_proveedor serial NOT NULL,
	nombre_proveedor varchar(100) NOT NULL,
	ruc_proveedor varchar(15) NOT NULL,
	telefono_proveedor varchar(10) NOT NULL,
	direccion_proveedor varchar(150) NOT NULL,
	correo_proveedor varchar(100) NOT NULL,
	id_rol_proveedor_rol_proveedor integer,
	apellido_proveedor varchar(100) NOT NULL,
	id_ciudad_ciudad integer,
	CONSTRAINT "PROVEEDOR_FLORES_pk" PRIMARY KEY (id_proveedor)

);
-- ddl-end --
ALTER TABLE public.proveedor OWNER TO postgres;
-- ddl-end --

-- object: public.ciudad | type: TABLE --
-- DROP TABLE IF EXISTS public.ciudad CASCADE;
CREATE TABLE public.ciudad(
	id_ciudad serial NOT NULL,
	nombre_ciudad varchar(50) NOT NULL,
	CONSTRAINT "CIUDAD_pk" PRIMARY KEY (id_ciudad)

);
-- ddl-end --
COMMENT ON TABLE public.ciudad IS 'Almacena las ciudades de los paises registrados';
-- ddl-end --
ALTER TABLE public.ciudad OWNER TO postgres;
-- ddl-end --

-- object: public.longitudes | type: TABLE --
-- DROP TABLE IF EXISTS public.longitudes CASCADE;
CREATE TABLE public.longitudes(
	id_longitud serial NOT NULL,
	cantidad_longitud numeric(20) NOT NULL,
	sigla_longitud varchar(5) NOT NULL,
	CONSTRAINT "pk_LONGITUDES" PRIMARY KEY (id_longitud)

);
-- ddl-end --
COMMENT ON TABLE public.longitudes IS 'Almacena las longitudes de los tallos de las flores ';
-- ddl-end --
ALTER TABLE public.longitudes OWNER TO postgres;
-- ddl-end --

-- object: public.inventario_flores | type: TABLE --
-- DROP TABLE IF EXISTS public.inventario_flores CASCADE;
CREATE TABLE public.inventario_flores(
	id_inv_flores serial NOT NULL,
	fecha_inv_flores date NOT NULL,
	hora_inv_flores time NOT NULL,
	id_clasificacion_clasificacion integer,
	id_tipo_venta_tipo_venta integer,
	CONSTRAINT "pk_INVENTARIO_FLORES" PRIMARY KEY (id_inv_flores)

);
-- ddl-end --
COMMENT ON TABLE public.inventario_flores IS 'Almacena todas las flores que se tiene actualmente';
-- ddl-end --
COMMENT ON CONSTRAINT "pk_INVENTARIO_FLORES" ON public.inventario_flores  IS 'Clave primaria de la tabla INVENTARIO_FLORES';
-- ddl-end --
ALTER TABLE public.inventario_flores OWNER TO postgres;
-- ddl-end --

-- object: public.variedad_flores | type: TABLE --
-- DROP TABLE IF EXISTS public.variedad_flores CASCADE;
CREATE TABLE public.variedad_flores(
	id_variedad_flores serial NOT NULL,
	nombre_variedad_flores varchar(200) NOT NULL,
	CONSTRAINT "pk_VARIEDAD_FLORES" PRIMARY KEY (id_variedad_flores)

);
-- ddl-end --
COMMENT ON TABLE public.variedad_flores IS 'Almacena la  variedad de las flores ';
-- ddl-end --
ALTER TABLE public.variedad_flores OWNER TO postgres;
-- ddl-end --

-- object: public.pedido_flores | type: TABLE --
-- DROP TABLE IF EXISTS public.pedido_flores CASCADE;
CREATE TABLE public.pedido_flores(
	id_pedido_flores serial NOT NULL,
	fecha_pedido_flores date NOT NULL,
	fecha_entrega_flores date NOT NULL,
	cantidad_tallos_pedido numeric(50) NOT NULL,
	id_variedad_flores_variedad_flores integer,
	id_longitud_longitudes integer,
	id_proveedor_proveedor integer,
	valor_pedido_flores money,
	CONSTRAINT "pk_PEDIDO_FLORES" PRIMARY KEY (id_pedido_flores)

);
-- ddl-end --
COMMENT ON TABLE public.pedido_flores IS 'Se almacena los pedidos de las flores realizadas al proveedor';
-- ddl-end --
COMMENT ON COLUMN public.pedido_flores.cantidad_tallos_pedido IS 'Almacena la cantidad de los tallos que se pidió al proveedor';
-- ddl-end --
ALTER TABLE public.pedido_flores OWNER TO postgres;
-- ddl-end --

-- object: longitudes_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_flores DROP CONSTRAINT IF EXISTS longitudes_fk CASCADE;
ALTER TABLE public.pedido_flores ADD CONSTRAINT longitudes_fk FOREIGN KEY (id_longitud_longitudes)
REFERENCES public.longitudes (id_longitud) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: variedad_flores_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_flores DROP CONSTRAINT IF EXISTS variedad_flores_fk CASCADE;
ALTER TABLE public.pedido_flores ADD CONSTRAINT variedad_flores_fk FOREIGN KEY (id_variedad_flores_variedad_flores)
REFERENCES public.variedad_flores (id_variedad_flores) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: pedido_flores_uq | type: CONSTRAINT --
-- ALTER TABLE public.pedido_flores DROP CONSTRAINT IF EXISTS pedido_flores_uq CASCADE;
ALTER TABLE public.pedido_flores ADD CONSTRAINT pedido_flores_uq UNIQUE (id_variedad_flores_variedad_flores);
-- ddl-end --

-- object: proveedor_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_flores DROP CONSTRAINT IF EXISTS proveedor_fk CASCADE;
ALTER TABLE public.pedido_flores ADD CONSTRAINT proveedor_fk FOREIGN KEY (id_proveedor_proveedor)
REFERENCES public.proveedor (id_proveedor) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.estado_flores | type: TABLE --
-- DROP TABLE IF EXISTS public.estado_flores CASCADE;
CREATE TABLE public.estado_flores(
	id_estado_flores serial NOT NULL,
	nombre_estado_flores varchar(50) NOT NULL,
	CONSTRAINT "pk_ESTADO_FLORES" PRIMARY KEY (id_estado_flores)

);
-- ddl-end --
COMMENT ON CONSTRAINT "pk_ESTADO_FLORES" ON public.estado_flores  IS 'Clave primaria de la tabla ESTADO_FLORES';
-- ddl-end --
ALTER TABLE public.estado_flores OWNER TO postgres;
-- ddl-end --

-- object: public.clasificacion | type: TABLE --
-- DROP TABLE IF EXISTS public.clasificacion CASCADE;
CREATE TABLE public.clasificacion(
	id_clasificacion serial NOT NULL,
	cantidad_clasificacion numeric(10) NOT NULL,
	id_pedido_flores_pedido_flores integer,
	id_estado_flores_estado_flores integer,
	CONSTRAINT "pk_CLASIFICACION" PRIMARY KEY (id_clasificacion)

);
-- ddl-end --
COMMENT ON TABLE public.clasificacion IS 'Almacena las flores que ingresan a la empresa';
-- ddl-end --
COMMENT ON CONSTRAINT "pk_CLASIFICACION" ON public.clasificacion  IS 'Clave primaria de la tabla clasificacion';
-- ddl-end --
ALTER TABLE public.clasificacion OWNER TO postgres;
-- ddl-end --

-- object: estado_flores_fk | type: CONSTRAINT --
-- ALTER TABLE public.clasificacion DROP CONSTRAINT IF EXISTS estado_flores_fk CASCADE;
ALTER TABLE public.clasificacion ADD CONSTRAINT estado_flores_fk FOREIGN KEY (id_estado_flores_estado_flores)
REFERENCES public.estado_flores (id_estado_flores) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: pedido_flores_fk | type: CONSTRAINT --
-- ALTER TABLE public.clasificacion DROP CONSTRAINT IF EXISTS pedido_flores_fk CASCADE;
ALTER TABLE public.clasificacion ADD CONSTRAINT pedido_flores_fk FOREIGN KEY (id_pedido_flores_pedido_flores)
REFERENCES public.pedido_flores (id_pedido_flores) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.tipo_venta | type: TABLE --
-- DROP TABLE IF EXISTS public.tipo_venta CASCADE;
CREATE TABLE public.tipo_venta(
	id_tipo_venta serial NOT NULL,
	nombre_tipo_venta varchar(50) NOT NULL,
	CONSTRAINT "pk_TIPO_VENTA" PRIMARY KEY (id_tipo_venta)

);
-- ddl-end --
COMMENT ON COLUMN public.tipo_venta.nombre_tipo_venta IS 'Nacional
Expostacion';
-- ddl-end --
ALTER TABLE public.tipo_venta OWNER TO postgres;
-- ddl-end --

-- object: clasificacion_fk | type: CONSTRAINT --
-- ALTER TABLE public.inventario_flores DROP CONSTRAINT IF EXISTS clasificacion_fk CASCADE;
ALTER TABLE public.inventario_flores ADD CONSTRAINT clasificacion_fk FOREIGN KEY (id_clasificacion_clasificacion)
REFERENCES public.clasificacion (id_clasificacion) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: tipo_venta_fk | type: CONSTRAINT --
-- ALTER TABLE public.inventario_flores DROP CONSTRAINT IF EXISTS tipo_venta_fk CASCADE;
ALTER TABLE public.inventario_flores ADD CONSTRAINT tipo_venta_fk FOREIGN KEY (id_tipo_venta_tipo_venta)
REFERENCES public.tipo_venta (id_tipo_venta) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.rol_proveedor | type: TABLE --
-- DROP TABLE IF EXISTS public.rol_proveedor CASCADE;
CREATE TABLE public.rol_proveedor(
	id_rol_proveedor serial NOT NULL,
	nombre_rol_proveedor varchar(50) NOT NULL,
	CONSTRAINT "pk_ROL_PROVEEDOR" PRIMARY KEY (id_rol_proveedor)

);
-- ddl-end --
COMMENT ON TABLE public.rol_proveedor IS 'Almacena el rol de los proveedores si es de matriales o flores';
-- ddl-end --
COMMENT ON CONSTRAINT "pk_ROL_PROVEEDOR" ON public.rol_proveedor  IS 'Clave primaria de la tabla proveedor';
-- ddl-end --
ALTER TABLE public.rol_proveedor OWNER TO postgres;
-- ddl-end --

-- object: rol_proveedor_fk | type: CONSTRAINT --
-- ALTER TABLE public.proveedor DROP CONSTRAINT IF EXISTS rol_proveedor_fk CASCADE;
ALTER TABLE public.proveedor ADD CONSTRAINT rol_proveedor_fk FOREIGN KEY (id_rol_proveedor_rol_proveedor)
REFERENCES public.rol_proveedor (id_rol_proveedor) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: proveedor_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_materiales DROP CONSTRAINT IF EXISTS proveedor_fk CASCADE;
ALTER TABLE public.pedido_materiales ADD CONSTRAINT proveedor_fk FOREIGN KEY (id_proveedor_proveedor)
REFERENCES public.proveedor (id_proveedor) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.bitacora | type: TABLE --
-- DROP TABLE IF EXISTS public.bitacora CASCADE;
CREATE TABLE public.bitacora(
	codigo_evento integer NOT NULL,
	fecha_evento date NOT NULL,
	metodo_bitacora varchar(100) NOT NULL,
	descripcion_bitacora varchar(200) NOT NULL,
	"direccionIP_bitacora" varchar(20) NOT NULL,
	id_usuario_usuarios integer,
	CONSTRAINT "pk_BITACORA" PRIMARY KEY (codigo_evento)

);
-- ddl-end --
COMMENT ON TABLE public.bitacora IS 'Almacena el historial ';
-- ddl-end --
ALTER TABLE public.bitacora OWNER TO postgres;
-- ddl-end --

-- object: usuarios_fk | type: CONSTRAINT --
-- ALTER TABLE public.bitacora DROP CONSTRAINT IF EXISTS usuarios_fk CASCADE;
ALTER TABLE public.bitacora ADD CONSTRAINT usuarios_fk FOREIGN KEY (id_usuario_usuarios)
REFERENCES public.usuarios (id_usuario) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.asignar_roles | type: TABLE --
-- DROP TABLE IF EXISTS public.asignar_roles CASCADE;
CREATE TABLE public.asignar_roles(
	id_asignar serial NOT NULL,
	id_rol_roles integer,
	id_usuario_usuarios integer,
	CONSTRAINT pk_id_asignar PRIMARY KEY (id_asignar)

);
-- ddl-end --
COMMENT ON TABLE public.asignar_roles IS 'Asigna roles';
-- ddl-end --
ALTER TABLE public.asignar_roles OWNER TO postgres;
-- ddl-end --

-- object: roles_fk | type: CONSTRAINT --
-- ALTER TABLE public.asignar_roles DROP CONSTRAINT IF EXISTS roles_fk CASCADE;
ALTER TABLE public.asignar_roles ADD CONSTRAINT roles_fk FOREIGN KEY (id_rol_roles)
REFERENCES public.roles (id_rol) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: usuarios_fk | type: CONSTRAINT --
-- ALTER TABLE public.asignar_roles DROP CONSTRAINT IF EXISTS usuarios_fk CASCADE;
ALTER TABLE public.asignar_roles ADD CONSTRAINT usuarios_fk FOREIGN KEY (id_usuario_usuarios)
REFERENCES public.usuarios (id_usuario) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.materiales | type: TABLE --
-- DROP TABLE IF EXISTS public.materiales CASCADE;
CREATE TABLE public.materiales(
	id_materiales serial NOT NULL,
	nombre varchar(100) NOT NULL,
	descripcion varchar(200),
	fecha_elaboracion timestamp NOT NULL,
	fecha_caducidad smallint NOT NULL,
	id_riesgomateriales_riesgo_materiales integer,
	CONSTRAINT pk_id_materiales PRIMARY KEY (id_materiales)

);
-- ddl-end --
COMMENT ON TABLE public.materiales IS 'Almacena todos los materiales, para el cuidado de las flores';
-- ddl-end --
ALTER TABLE public.materiales OWNER TO postgres;
-- ddl-end --

-- object: public.riesgo_materiales | type: TABLE --
-- DROP TABLE IF EXISTS public.riesgo_materiales CASCADE;
CREATE TABLE public.riesgo_materiales(
	id_riesgomateriales serial NOT NULL,
	nombre varchar(100) NOT NULL,
	descripcion varchar(200),
	CONSTRAINT pk_id_riesgomateriales PRIMARY KEY (id_riesgomateriales)

);
-- ddl-end --
COMMENT ON TABLE public.riesgo_materiales IS 'Almacena los posibles riesgos de cada material';
-- ddl-end --
ALTER TABLE public.riesgo_materiales OWNER TO postgres;
-- ddl-end --

-- object: riesgo_materiales_fk | type: CONSTRAINT --
-- ALTER TABLE public.materiales DROP CONSTRAINT IF EXISTS riesgo_materiales_fk CASCADE;
ALTER TABLE public.materiales ADD CONSTRAINT riesgo_materiales_fk FOREIGN KEY (id_riesgomateriales_riesgo_materiales)
REFERENCES public.riesgo_materiales (id_riesgomateriales) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: materiales_fk | type: CONSTRAINT --
-- ALTER TABLE public.pedido_materiales DROP CONSTRAINT IF EXISTS materiales_fk CASCADE;
ALTER TABLE public.pedido_materiales ADD CONSTRAINT materiales_fk FOREIGN KEY (id_materiales_materiales)
REFERENCES public.materiales (id_materiales) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: uq_rol_roles | type: CONSTRAINT --
-- ALTER TABLE public.asignar_roles DROP CONSTRAINT IF EXISTS uq_rol_roles CASCADE;
ALTER TABLE public.asignar_roles ADD CONSTRAINT uq_rol_roles UNIQUE (id_rol_roles);
-- ddl-end --

-- object: uq_usuario_usuarios | type: CONSTRAINT --
-- ALTER TABLE public.asignar_roles DROP CONSTRAINT IF EXISTS uq_usuario_usuarios CASCADE;
ALTER TABLE public.asignar_roles ADD CONSTRAINT uq_usuario_usuarios UNIQUE (id_usuario_usuarios);
-- ddl-end --

-- object: ciudad_fk | type: CONSTRAINT --
-- ALTER TABLE public.usuarios DROP CONSTRAINT IF EXISTS ciudad_fk CASCADE;
ALTER TABLE public.usuarios ADD CONSTRAINT ciudad_fk FOREIGN KEY (id_ciudad_ciudad)
REFERENCES public.ciudad (id_ciudad) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: ciudad_fk | type: CONSTRAINT --
-- ALTER TABLE public.proveedor DROP CONSTRAINT IF EXISTS ciudad_fk CASCADE;
ALTER TABLE public.proveedor ADD CONSTRAINT ciudad_fk FOREIGN KEY (id_ciudad_ciudad)
REFERENCES public.ciudad (id_ciudad) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


